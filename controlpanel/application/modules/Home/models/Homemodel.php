<?PHP
class Homemodel extends CI_Model
{
	function insertData($tbl_name,$data_array,$sendid = NULL)
	{
	 	/*echo $tbl_name."<br/>";
	 	print_r($data_array);
	 	echo $sendid;
	 	exit;*/
	 	
	 	$this->db->insert($tbl_name,$data_array);
	 	$result_id = $this->db->insert_id();
	 	
	 	/*echo $result_id;
	 	exit;*/
	 	
	 	if($sendid == 1)
	 	{
	 		//return id
	 		return $result_id;
	 	}
	}
	
	function getdata($table, $select = "*", $condition = '') {
		$this->db->select($select);
		$this->db->from($table);
		if(!empty($condition)){
			$this->db->where("($condition)");
		}
		$query = $this->db->get();
		// echo $this->db->last_query();
		// exit;
		if($query->num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}

   	function updateRecord($tbl,$data,$condition)
   	{
	 	$this->db->where($condition);
     	if($this->db->update($tbl, $data)){
		  	return true;	
		}else{
		  	return false;
		} 
	}
	 
	function delRecord($tbl_name,$condition)
	{
		$this->db->where($condition);
	    if($this->db->delete($tbl_name)){
	    	return true;
	   	}else{
	     	return false;
	   	}
	}
 function checkfcmexist($userid)
	 {
             $query = $this->db->query("Select * From tbl_mobile_fcm where user_id=$userid");
             if($query->num_rows()>0)
			 {
                 return 1;
             }
             else
			 {
                 return 0;
             }
     }
		 function updatefcm($datar,$eid)
	{
		$this -> db -> where('user_id', $eid);
		$this -> db -> update('tbl_mobile_fcm',$datar);
		 
		if ($this->db->affected_rows() > 0)
			{
			  return true;
			}
		else
			{
			  return true;
			} 
		 
	}
 
	/*
	function get_messages($from_id,$to_id){
	    $sql = $this->db->query('Select *
	                      From tbl_messages 
	                      Where (from_id = '.$this->db->escape($from_id).' And to_id = '.$this->db->escape($to_id).') 
	                      OR
	                      (from_id = '.$this->db->escape($to_id).' And to_id = '.$this->db->escape($from_id).')
	                      order by message_id desc
	                      Limit 5
	                      ');
	    if($sql->num_rows() > 0){
	        return $sql->result_array();
	    }else{
	        return false;
	    }
	}
	*/
}
?>
