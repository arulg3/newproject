<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>LOCATION</h2>
            </div>
            <!-- Widgets -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					
					<div class="body">
						<ol class="breadcrumb align-right">
							<li><a href="<?php echo base_url("home");?>"><i class="material-icons">home</i> Home</a></li>
							<li><a href="<?php echo base_url($this->router->fetch_module());?>"><i class="material-icons">location_on</i>Location</a></li>
							<li class="active"><i class="material-icons">mode_edit</i> Add Edit</li>
						</ol>
					</div>
                    <div class="card">
                        <div class="header">
                            <h2>Add / Edit Location</h2>
                        </div>
                        <div class="body">
                            <form id="form-validate" class="form-validate" name="form-validate" method="POST">
								<input type="hidden" name="location_id" id="location_id" value="<?php echo(!empty($location_data['location_id']))?$location_data['location_id']:"";?>">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="location_name" id="location_name" value="<?php echo(!empty($location_data['location_name']))?$location_data['location_name']:"";?>" required>
                                        <label class="form-label">Location Name</label>
                                    </div>
                                    <div class="help-info"></div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="location_code" id="location_code" value="<?php echo(!empty($location_data['location_code']))?$location_data['location_code']:"";?>" required>
                                        <label class="form-label">Location Code</label>
                                    </div>
                                    <div class="help-info"></div>
                                </div>
								<div class="form-group form-float">
                                    <div class="form-line">
                                        <textarea class="form-control" name="location_address" id="location_address" required><?php echo(!empty($location_data['location_address']))?$location_data['location_address']:"";?></textarea>
                                        <label class="form-label">Location Address</label>
                                    </div>
                                    <div class="help-info"></div>
                                </div>
								<div class="form-group form-float">
                                    <div class="form-line">
                                       <br><select class="form-control" name="user_id[]" id="user_id" multiple >
										<option value="">Select Users</option>
										<?php if(!empty($user_data)){
											$existing_users = array();
											$disabled = "";
											if(!empty($location_users_data)){
												$existing_users = array_column($location_users_data,"user_id");
											}
											?>
											<?php foreach($user_data as $key=>$val){
											$disabled = ($val['status'] == 'In-active') ? "disabled = 'disabled'" : "";
											?>
											<option value="<?php echo $val['user_id']?>" <?php echo(in_array($val['user_id'],$existing_users))?"selected":"";?> <?php echo $disabled; ?> ><?php echo $val['first_name']." ".$val['last_name'];?></option>
											<?php }?>
										<?php }?>
										</select>
                                        <label class="form-label">Users </label>
                                    </div>
                                    <div class="help-info"></div>
                                </div>
                                <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                                <a href="<?php echo base_url($this->router->fetch_module());?>" class="btn btn-primary waves-effect" type="button">Cancel</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Widgets -->
        </div>
    </section>
<script>
	var vRules = {
		"location_name":{required:true},
		"location_code":{required:true,remote: {
							url:"<?php echo base_url($this->router->fetch_module());?>/dataExist" ,
							type: "post",
							data: {location_code: function() {return $( "#location_code" ).val();},location_id:function() {return $( "#location_id" ).val();}}
						}},
		"user_id[]":{required:true},
		"location_address":{required:true}

	};
	var vMessages = {
		"location_name":{required:"Please Enter the Location Name"},
		"location_code":{required:"Please Enter the Location Code",remote:"Location Code is Already Exist."},
		"location_address":{required:"Please Enter the Location Address"},
		"user_id[]":{required:"Please Select atleast one user "},
	}

	$("#form-validate").validate({
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form) 
		{
			var act = "<?php echo base_url();?>location/submitForm";
			$("#form-validate").ajaxSubmit({
				url: act, 
				type: 'post',
				dataType: 'json',
				cache: false,
				clearForm: false, 
				beforeSubmit : function(arr, $form, options){
					$(".btn-primary").hide();
				},
				success: function (response) 
				{
					$(".btn-primary").show();
					if(response.success){
					    swal({title: "Success!",text:response.msg,confirmButtonColor: "#a81f3b",imageUrl: "<?php echo base_url('assets/images/thumbs-up.png');?>"});
						setTimeout(function(){
							window.location = "<?php echo base_url($this->router->fetch_module());?>";
						},2000);
					}else{	
						swal(response.msg);
						return false;
					}
				}
			});
		}
	});
</script>