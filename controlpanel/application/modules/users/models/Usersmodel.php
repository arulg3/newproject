<?PHP
class Usersmodel extends CI_Model
{
	function getRecords($get){
		$table = "tbl_users";
		$table_id = 'user_id';
		$default_sort_column = 'user_id';
		$default_sort_order = 'desc';
		// $condition = "i.user_type = 2 ";
		$condition = " 1=1 and user_type !=1";
		$colArray = array('i.first_name','i.last_name','i.email_id','i.phone_number','i.address','i.status');
		$sortArray = array('i.first_name','i.last_name','i.email_id','i.phone_number','i.address','i.status');
		
		$page = $get['iDisplayStart'];	// iDisplayStart starting offset of limit funciton
		$rows = $get['iDisplayLength'];	// iDisplayLength no of records from the offset
		
		// sort order by column
		$sort = isset($get['iSortCol_0']) ? strval($sortArray[$get['iSortCol_0']]) : $default_sort_column;  
		$order = isset($get['sSortDir_0']) ? strval($get['sSortDir_0']) : $default_sort_order;
        

		for($i=0;$i<6;$i++){
			if(isset($_GET['Searchkey_'.$i]) && $_GET['Searchkey_'.$i] !=""){
			    if($i == 5){
			        $condition .= " AND ".$colArray[$i]." = '".$_GET['Searchkey_'.$i]."' ";
			    }else{
			        $condition .= " AND ".$colArray[$i]." like '%".$_GET['Searchkey_'.$i]."%' ";
			    }
			}
		}
		
		$this -> db -> select('i.*');
		$this -> db -> from("$table as i");
		$this->db->where("($condition)");
		$this->db->order_by($sort, $order);
		$this->db->limit($rows,$page);
		
		$query = $this -> db -> get();
		
		$this -> db -> select('i.*');
		$this -> db -> from("$table as i");
		$this->db->where("($condition)");
		$this->db->order_by($sort, $order);
		
		$query1 = $this -> db -> get();
		
		if($query -> num_rows() > 0){
			$totcount = $query1 -> num_rows();
			return array("query_result" => $query->result(), "totalRecords" => $totcount);
		}else{
			return array("totalRecords" => 0);
		}
	}


}
?>
