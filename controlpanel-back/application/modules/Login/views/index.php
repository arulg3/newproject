    <div class="login-box">
        <div class="card">
            <div class="body">
                 <div class="logo">
                    <a href="javascript:void(0);"><img src="<?php echo base_url("assets/images/logo.png");?>" width="200px"></a>
                </div>
                <div class="login-from-wrapper zoomIn">
                    <form id="form-login" method="POST">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">person</i>
                            </span>
                            <div class="form-line">
                                <input type="text" class="form-control" name="username" id="username" placeholder="Username" required autofocus>
                            </div>
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">lock</i>
                            </span>
                            <div class="form-line">
                                <input type="password" class="form-control" name="password" placeholder="Password" required>
                            </div>
                        </div>
                        <div class="row" id="div_response_msg">
                            <div class="col-xs-10 p-t-5">
                                <p class="font-bold font-italic col-teal" id="response_msg"></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-8 p-t-5">
                                <input type="checkbox" name="rememberme" id="rememberme" class="filled-in">
                                <label for="rememberme">Remember Me</label>
                            </div>
                            <div class="col-xs-12">
                                <button class="btn btn-block btn-primary waves-effect" type="submit">SIGN IN</button>
                            </div>
                        </div>
                        <div class="row m-t-15 m-b--20">
                            <div class="col-xs-12 align-right">
                                <a id="toForgot" href="#">Forgot Password?</a>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="forget-from-wrapper " style="display: none">
                    <form class="forget_form" id="forget_form" method="POST">
                        <h3 class="forgotpassword-head">
                            <i class="fa fa-lg fa-fw fa-lock"></i>Forgot Password ?
                        </h3>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">email</i>
                            </span>
                            <div class="form-line">
                                <input type="email" class="form-control" name="email_id" placeholder="Email" required autofocus >
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <button type="submit" class="btn btn-primary btn-block btn-reset">RESET</button>
                            </div>
                        </div>
                        <br>
                        <div class="row" id="div_response_msg2">
                            <div class="col-xs-10 p-t-5">
                                <p class="font-bold font-italic col-teal" id="response_msg2"></p>
                            </div>
                        </div>
                        <p class="semibold-text mb-0 mt-10"><a id="toLogin" href="#"><i class="fa fa-angle-left fa-fw"></i> Back to Login</a></p>
                        <div class="clearfix"></div>
                        <div id="show_msg1"></div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
<style type="text/css">
    #div_response_msg{
        display: none;
    }
</style>
<script>
$("#form-login").validate({
    rules: {},
    messages: {},
    submitHandler: function(form) 
    {
        var act = "<?php echo base_url();?>login/loginValidate";
        $("#form-login").ajaxSubmit({
            url: act, 
            type: 'post',
            dataType: 'json',
            cache: false,
            clearForm: false,
            beforeSubmit:function(){
                //$(".btn-primary").prop("disabled","true");
            },
            success: function (response) {
               // $(".btn-primary").prop("disabled","false");
                // $(".btn-primary").removeAttr("disabled");
                if(response.success){
                    swal({title: "Success!",text:response.msg,timer: 3000,showConfirmButton: false,imageUrl: "<?php echo base_url('assets/images/thumbs-up.png');?>"});

                    $("#response_msg").html(response.msg);
                    $("#div_response_msg").show();
                    $("#div_response_msg").fadeOut(1000);

                    setTimeout(function(){
                        window.location = "<?php echo base_url();?>home";
                    },1000);

                }else{  
                    swal(response.msg);
                    return false;
                }

            }
        });
    }
});

// Jitendra new code starts here
$("#forget_form").validate({
    rules:{},
    messages:{},
    submitHandler:function(form)
    {
        var act="<?php echo base_url();?>login/forgotpassword";
        $("#forget_form").ajaxSubmit({
           url: act, 
            type: 'post',
            dataType: 'json',
            cache: false,
            clearForm: false,
            beforeSubmit:function(){
                //$(".btn-reset").attr("disabled","true");
            },
            success : function(response){
                //alert(response);
                //return false;
		//$(".btn-reset").prop("disabled","false");
                if(response.success){
                    $("#response_msg2").html(response.msg);
                    $("#div_response_msg2").show();
                    $("#div_response_msg2").fadeOut(1000);

                    setTimeout(function(){
                        window.location="<?php echo base_url();?>home";
                    },1000);
                }else{
                    swal(response.msg);
                    return false;
                }
            }
        });
    }
});

$('#toLogin').click(function(){
$('.forget-from-wrapper').hide();
$('.login-from-wrapper').fadeIn(100);
});

$('#toForgot').click(function(){
$('.login-from-wrapper').hide();
$('.forget-from-wrapper').fadeIn(100);
});
</script>
</html>
