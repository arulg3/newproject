<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Category</h2>
            </div>
            <!-- Widgets -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					
					<div class="body">
						<ol class="breadcrumb align-right">
							<li><a href="<?php echo base_url("home");?>"><i class="material-icons">home</i> Home</a></li>
							<li class="active"><i class="material-icons">shopping_basket</i>Product Master</li>
							<li><a href="<?php echo base_url($this->router->fetch_module());?>"><i class="material-icons">list</i> Category</a></li>
							<li class="active"><i class="material-icons">mode_edit</i>Add Edit</li>
						</ol>
					</div>
                    <div class="card">
                        <div class="header">
                            <h2>Add / Edit Users</h2>
                        </div>
                        <div class="body">
                            <form id="form-validate" class="form-validate" name="form-validate" method="POST" enctype="multipart/form-data">
								<input type="hidden" name="category_id" id="category_id" value="<?php echo(!empty($category_data['category_id']))?$category_data['category_id']:"";?>">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="category_name" id="category_name" value="<?php echo(!empty($category_data['category_name']))?$category_data['category_name']:"";?>" required>
                                        <label class="form-label">Category Name</label>
                                    </div>
                                    <div class="help-info"></div>
                                </div>
								
                                <div class="form-group form-float">
									<label class="form-label">Category Image</label>
                                    <div class="form-line">
                                        <input type="file" class="form-control" name="category_image" id="category_image">
                                    </div>
									<?php if(!empty($category_data['category_image'])){?>
										<div class="imagewrap">
											<button type="button" class="btn btn-primary image_remove_icon waves-effect" onclick = "deleteFile('<?php echo $category_data['category_id']?>','<?php echo $category_data['category_image'];?>')"><i class="material-icons" >delete_forever</i></button>
											<img src="<?php echo FRONT_URL."/uploads/category_images/".$category_data['category_image'];?>" class="preview_uploaded_image">
										</div>
										<input type="hidden" name="existing_category_image" id="existing_category_image" value="<?php echo(!empty($category_data['category_image']))?$category_data['category_image']:"";?>">
									<?php }?>
                                    <div class="help-info"></div>
								</div>
                                <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                                <a href="<?php echo base_url($this->router->fetch_module());?>" class="btn btn-primary waves-effect" type="button">Cancel</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Widgets -->
        </div>
    </section>
<script>
	var vRules = {
		"category_name":{required:true,remote: {
							url:"<?php echo base_url($this->router->fetch_module());?>/dataExist" ,
							type: "post",
							data: {category_name: function() {return $( "#category_name" ).val();},category_id:function() {return $( "#category_id" ).val();}}
						}},
	};
	var vMessages = {
		"category_name":{required:"Please Enter the Category Name",remote:"Category name already exist."},
	};

	$("#form-validate").validate({
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form) 
		{
			var act = "<?php echo base_url($this->router->fetch_module());?>/submitForm";
			$("#form-validate").ajaxSubmit({
				url: act, 
				type: 'post',
				dataType: 'json',
				cache: false,
				clearForm: false, 
				beforeSubmit : function(arr, $form, options){
					$(".btn-primary").hide();
				},
				success: function (response) 
				{
					$(".btn-primary").show();
					if(response.success)
					{
					    swal({title: "Success!",text:response.msg,confirmButtonColor: "#a81f3b",imageUrl: "<?php echo base_url('assets/images/thumbs-up.png');?>"});
						setTimeout(function(){
							window.location = "<?php echo base_url($this->router->fetch_module());?>";
						},2000);
					}else{	
						swal(response.msg);
						return false;
					}
				}
			});
		}
	});
</script>
<script>
function deleteFile(id,name) {
	swal({
		title: "Are you sure?",
		text: "To Delete this File.",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#a81f3b",
		confirmButtonText: "Yes, Delete it !",
		closeOnConfirm: false
	}, function () {
		$.ajax({
			url: "<?php echo base_url($this->uri->segment(1))?>/deleteFile",
			async: false,
			data : { id : id,name: name},
			type: "POST",
			dataType: "json",
			success: function (response){
				if(response.success){
					swal({title: "Success!",text: "File Deleted Successfully.",confirmButtonColor: "#a81f3b",imageUrl: "<?php echo base_url('assets/images/thumbs-up.png');?>"});
					setTimeout(function(){
						window.location = "<?php echo base_url($this->router->fetch_module());?>";
					},2000);
				}else{
					swal("Problem in Deleting File!");
					return false;
				}
			}
		});
		
	});
}
</script>