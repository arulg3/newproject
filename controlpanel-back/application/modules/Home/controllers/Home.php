<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Home extends CI_Controller 
{	
	function __construct(){
		parent::__construct();
		$this->load->model('homemodel','',TRUE);
		checklogin();
	}
 
	function index(){
		$this->load->view('template/header.php');
		$this->load->view('home/index');
		$this->load->view('template/footer.php');
	}
	function logout(){
	    if(!empty($_SESSION['meat_control_admin'])){
	        unset($_SESSION['meat_control_admin']);
	    }
	    session_destroy();
	    redirect("login");
	}
	
	function savefcm(){
		$fcm=$_POST['fcm'];
		$userid=$_SESSION["meat_control_admin"][0]['user_id'];
		$getcount=$this->homemodel->checkfcmexist($userid);
		$data=array();
		$data['fcm_token']=$fcm;
		$data['user_type']=$_SESSION["meat_control_admin"][0]['user_type'];
		$data['user_id']=$_SESSION["meat_control_admin"][0]['user_id'];
		if($getcount>0){
			$result = $this->homemodel->updatefcm($data,$_SESSION["set_user"][0]['user_id']);	
		}else{
			$result = $this->homemodel->insertData('tbl_mobile_fcm',$data,'1');	
		}
		exit;
	}
}

?>
