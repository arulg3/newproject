// document ready function
var asInitVals = new Array();
var oTable;
$(document).ready(function() { 	
	//--------------- Data tables ------------------//
	var filename = window.location.pathname.substr(window.location.pathname.lastIndexOf("/")+1);
	
	var bFilter = true;
    if($('table').hasClass('nofilter')){
        bFilter = false;
    }
    var columnSort = new Array; 
    $(this).find('thead tr th').each(function(){
        if($(this).attr('data-bSortable') == 'false') {
            columnSort.push({ "bSortable": false });
        } else {
            if($(this).html() == "Action" || $(this).html() == "Actions")
        	{
            	columnSort.push({ "bSortable": false });
        	}else{
        		columnSort.push({ "bSortable": true });
        	}
        }
    });
	
    /* only for bootstrap select if used in filters */
    $(".bootstrap-select").removeClass("searchInput");
	if($('table').hasClass('dynamicTable')){
		var method = $(".dynamicTable").attr("callfunction");
		if(method == "" || method == null){
			method = filename+"/fetch";
		}
		var resposive_flag = true;
		// alert($( window ).width());
		if($( window ).width() > 600){
			resposive_flag = false;
			$('table').closest("div").addClass("table-responsive");
		}else{
			$('table').closest("div").removeClass("table-responsive");
		}
		noofrecords = $(".dynamicTable").attr("noofrecords");		
		oTable = $('.dynamicTable').dataTable({
			// rowReorder: {
				// selector: 'td:nth-child(2)'
			// },
			"responsive": resposive_flag,
			"sPaginationType": "full_numbers",			
			"bJQueryUI": false,
			"bAutoWidth": false,
			"bLengthChange": false,
			"bProcessing": true,
			"bServerSide": true,
			"iDisplayLength":noofrecords,
			"aaSorting":[],
			// "sAjaxSource": "./"+filename+"/fetch?act=fetch",
			"sAjaxSource":method,
			"fnInitComplete": function(oSettings, json) {
				$('.dataTables_filter>label>input').parent().remove();				
		    },
		    "aoColumnDefs": [{ "bSortable": bFilter, "aTargets": [ -1 ] }],
		    "aoColumns": columnSort,
		    "fnServerParams": function ( aoData ) {
		    	var searchCount = 0;
		    	$(".searchInput").each(function(){
		    		aoData.push( { "name": "Searchkey_"+searchCount, "value": $(this).val() } );
		    		searchCount++;
		    	});
		     },
		    "fnDrawCallback": function( oSettings ) {
		    	if (typeof datatablecomplete == 'function') { 
		    		datatablecomplete("dynamicTable");
		    	} 
		    }	
		});

		$(".dataTables_filter select").bind("change", function()  {
			/* Filter on the column (the index) of this element */
			oTable.fnFilter( this.value, oTable.oApi._fnVisibleToColumnIndex(oTable.fnSettings(), $(".searchInput").index(this) ) );
		});
		$(".dataTables_filter input").keyup( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnFilter( this.value, oTable.oApi._fnVisibleToColumnIndex(oTable.fnSettings(), $(".searchInput").index(this) ) );
		});
		$(".dataTables_filter input").change( function () {				
			oTable.fnFilter( this.value, oTable.oApi._fnVisibleToColumnIndex(oTable.fnSettings(), $(".searchInput").index(this) ) );
		});	
		$(".dataTables_filter .hasDatepicker").on("dp.change", function(e) {
			oTable.fnFilter( this.value, oTable.oApi._fnVisibleToColumnIndex(oTable.fnSettings(), $(".searchInput").index(this) ) );
	  	});
					
	}
});

/* CLEAR SEARCH FILTER START*/
function clearSearchFilters(){
/*
* Clear Filters and refresh data table
* First it clear all the fields and call its event
*/ 
	$('.dataTables_filter input').val('');
	$('.dataTables_filter select').prop('selectedIndex',0);
	$('.dataTables_filter bootstrap-select').prop('selectedIndex',0);
	$('select').selectpicker('refresh')
	if($('select').hasClass('select2')){
		$('.select2').val("");
		$('.select2').trigger('change.select2');
	}

	var oSettings = oTable.fnSettings();
	for(iCol = 0; iCol < oSettings.aoPreSearchCols.length; iCol++) {
		oSettings.aoPreSearchCols[ iCol ].sSearch = '';
	}
	oTable.fnDraw();
}
/* CLEAR SEARCH FILTER END*/