<!DOCTYPE html>
<html>
<head>

<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>NM</title>
    
    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,700,600' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'>
    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url("assets");?>/plugins/owlcarousel/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url("assets");?>/plugins/intl-tel-input/css/intlTelInput.css">
    <link rel="stylesheet" href="<?php echo base_url("assets");?>/plugins/sweetalert/sweetalert.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo base_url("assets");?>/plugins/aos/aos.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo base_url("assets");?>/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url("assets");?>/css/responsive.css">
    <link rel="stylesheet" href="<?php echo base_url("assets");?>/css/animate.css">


	<!-- All css files are included here. -->
    <!-- Stylesheets -->
    <link rel="stylesheet" href="<?php echo base_url("assets");?>/plugins/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <!-- Cusom css -->
    <link rel="stylesheet" href="<?php echo base_url("assets");?>/css/main.css">

    <!-- Latest jQuery form server -->
    <script src="<?php echo base_url("assets");?>/js/jquery.js"></script>
    <script src="<?php echo base_url("assets");?>/plugins/jquery-validation/jquery.validate.min.js"></script>
    <script src="<?php echo base_url("assets");?>/plugins/jquery-validation/jquery.form.js"></script>
    <script src="<?php echo base_url("assets");?>/js/jquery.sticky.js"></script>
    <script src="<?php echo base_url("assets");?>/js/jquery.easing.1.3.min.js"></script>
    <script src="<?php echo base_url("assets");?>/plugins/owlcarousel/owl.carousel.min.js"></script>
    <script src="<?php echo base_url("assets");?>/plugins/intl-tel-input/js/intlTelInput.js"></script>
    <script src="<?php echo base_url("assets");?>/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo base_url("assets");?>/plugins/sweetalert/sweetalert-dev.js"></script>
    <script src="<?php echo base_url("assets");?>/plugins/aos/aos.js"></script>
    
    <!-- Bootstrap JS form CDN -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    
    <!-- Main Script -->
    <script src="<?php echo base_url("assets");?>/js/main.js"></script>
</head>
    <body>
        <header>
            <div class="header-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 pull-right">
                            <div class="header-right">
                                <ul class="list-unstyled list-inline">
                                    <li class="dropdown dropdown-small">
                                        <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#"><img class="img-language" src="<?php echo base_url("assets/images/language.png"); ?>"><span class="key">Language :</span><span class="value"><img class="img-language selected-img-language" src="<?php echo base_url("assets/images/english_flag.png"); ?>">English </span><b class="caret"></b></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#"><img class="img-language" src="<?php echo base_url("assets/images/english_flag.png"); ?>">English</a></li>
                                            <li><a href="#"><img class="img-language" src="<?php echo base_url("assets/images/chinese_flag.png"); ?>">Chinese</a></li>
                                            <li><a href="#"><img class="img-language thai-lang-img" src="<?php echo base_url("assets/images/thai_flag.png"); ?>">Thai</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown dropdown-small">
                                        <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#"><span class="key">Login <b class="caret"></b></a>
                                        <ul class="dropdown-menu">
                                            <?php if(!empty($_SESSION['wwmeat_user'])){?>
                                                <li><a href="<?php echo base_url("myprofile")?>">My Account</a></li>
                                                <li><a href="<?php echo base_url("register/logout")?>">Logout</a></li>
                                            <?php }else{ ?>
                                                <li>
                                                    <a href="javascript:void(0);" data-toggle="modal" data-target=".exporter-login-register-form">
                                                        LOGIN AS EXPORTER
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0);" data-toggle="modal" data-target=".buyer-login-register-form">
                                                        LOGIN AS BUYER
                                                    </a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- End header area -->
            <div class="mainmenu-area">
                <div class="container">
                    <div class="col-sm-4 col-xs-8">
                        <div class="logo">
                            <a href="<?php echo base_url();?>" class="logo-title"><span>Worldwidemeats</span></a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-sm-8 col-xs-4">
                        <div class="row pull-right">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div> 
                            <div class="navbar-collapse collapse">
                                <ul class="nav navbar-nav ">
                                    <li class="<?php echo($this->uri->segment(1) == "home" || $this->uri->segment(1) == "")?"active":"";?>"><a href="<?php echo base_url();?>">Home</a></li>
                                    <li class="dropdown main-menu-dropdown" >
                                        <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#">Products</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Buffalo Product</a></li>
                                            <li><a href="#">Lamb products</a></li>
                                        </ul>
                                    </li>
                                    <li class="<?php echo($this->uri->segment(1) == "aboutus")?"active":"";?>"><a href="<?php echo base_url("aboutus");?>">About us</a></li>
                                    <li class="<?php echo($this->uri->segment(1) == "contactus")?"active":"";?>"><a href="#">Contact</a></li>
                                </ul>
                            </div>  
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div> <!-- End mainmenu area -->
        </header>
        <!-- exporter login model -->
        <div class="modal fade exporter-login-register-form" role="dialog">
            <div class="modal-dialog width100">
                <div class="modal-content modal-lg shsdow-effect">
                    <div class="login-model-wrapper ">
                        <div class="left col-sm-6 col-xs-12 col-lg-6 col-md-6">
                            <img src="https://colorlib.com/etc/lf/Login_v1/images/img-01.png" alt="">
                        </div>
                        <div class="right col-sm-6 col-xs-12 col-lg-6 col-md-6">
                            <form action="#" name="exporter_login"  id="exporter_login" method="post" enctype="multipart/form-data" >
                                <input type="hidden" name="user_type" value="3">
                                <div class="col-sm-12 close-button-wrapper">
                                    <img src="<?php echo base_url("assets/images/cross.png")?>" alt="" data-dismiss="modal">
                                </div>
                                <h1 class="text-center">Login</h1>
                                <div class="form-group ">
                                    <label for="">Username</label>
                                    <input type="text" class="form-control" name="username" id="username">
                                </div>
                                <div class="form-group ">
                                    <label for="">Password</label>
                                    <input type="password" class="form-control" name="password" id="password">
                                </div>
                                <div class="form-group ">
                                    <label for="">
                                        <input type="checkbox" name="" id="">
                                        Remember Password
                                    </label>
                                </div>
                                <div class="form-group text-center">
                                    <button class="btn btn-primary btn-block">
                                        Login
                                    </button>
                                </div>
                                <a href="<?php echo base_url()."register?user=exporter" ?>"> Register as New user <i class="fa fa-arrow-right"></i></a>
                            </form>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <!-- <div class="modal-content">
                    </div> -->
                </div>
            </div>
        </div>
        <!-- buyer login model -->
        <div class="modal fade buyer-login-register-form" role="dialog">
            <div class="modal-dialog width100">
                <div class="modal-content modal-lg shsdow-effect">
                    <div class="login-model-wrapper ">
                        <div class="left col-sm-6 col-xs-12 col-lg-6 col-md-6">
                            <img src="https://colorlib.com/etc/lf/Login_v1/images/img-01.png" alt="">
                        </div>
                        <div class="right col-sm-6 col-xs-12 col-lg-6 col-md-6">
                            <form action="#" name="buyer_login"  id="buyer_login" method="post" enctype="multipart/form-data" >
                                <input type="hidden" name="user_type" value="4">
                                <div class="col-sm-12 close-button-wrapper">
                                    <img src="<?php echo base_url("assets/images/cross.png")?>" alt="" data-dismiss="modal">
                                </div>
                                <h1 class="text-center">Login</h1>
                                <div class="form-group ">
                                    <label for="">Username</label>
                                    <input type="text" class="form-control" name="username" id="username">
                                </div>
                                <div class="form-group ">
                                    <label for="">Password</label>
                                    <input type="password" class="form-control" name="password" id="password">
                                </div>
                                <div class="form-group ">
                                    <label for="">
                                        <input type="checkbox" name="" id="">
                                        Remember Password
                                    </label>
                                </div>
                                <div class="form-group ">
                                    <button class="btn btn-primary btn-block">
                                        Login
                                    </button>
                                </div>
                                <a href="<?php echo base_url()."register?user=buyer" ?>"> Register as New user <i class="fa fa-arrow-right"></i></a>
                            </form>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
<script>
$(document).ready(function(){
    $("#buyer_login").validate({
        rules: {
            username:{required:true},
            password:{required:true}
        },
        messages: {
            username:{required:"Please enter username"},
            password:{required:"Please enter password"}
        },
        submitHandler: function(form){
            var act = "<?php echo base_url();?>register/loginValidate";
            $(form).ajaxSubmit({
                url: act, 
                type: 'post',
                dataType: 'json',
                cache: false,
                clearForm: false,
                success: function (response) {
                    if(response.success){
                        swal({title: "Success!",text:response.msg,timer: 3000,showConfirmButton: false,imageUrl: "<?php echo base_url('assets/images/thumbs-up.png');?>"});
                        $("#response_msg").html(response.msg);
                        $("#div_response_msg").show();
                        $("#div_response_msg").fadeOut(1000);
                        setTimeout(function(){
                            window.location = "<?php echo base_url();?>home";
                        },1000);
                    }else{  
                        swal(response.msg);
                        return false;
                    }
                }
            });
        }
    });
    $("#exporter_login").validate({
        rules: {
            username:{required:true},
            password:{required:true}
        },
        messages: {
            username:{required:"Please enter username"},
            password:{required:"Please enter password"}
        },
        submitHandler: function(form){
            var act = "<?php echo base_url();?>register/loginValidate";
            $(form).ajaxSubmit({
                url: act, 
                type: 'post',
                dataType: 'json',
                cache: false,
                clearForm: false,
                success: function (response) {
                    if(response.success){
                        swal({title: "Success!",text:response.msg,timer: 3000,showConfirmButton: false,imageUrl: "<?php echo base_url('assets/images/thumbs-up.png');?>"});
                        $("#response_msg").html(response.msg);
                        $("#div_response_msg").show();
                        $("#div_response_msg").fadeOut(1000);
                        setTimeout(function(){
                            window.location = "<?php echo base_url();?>home";
                        },1000);
                    }else{  
                        swal(response.msg);
                        return false;
                    }
                }
            });
        }
    });
})
</script>