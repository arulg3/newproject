<?PHP
class Common_model extends CI_Model {

	function getData($table,$select = "*", $condition = '', $order_by = "",$order = ""){
		$this->db->select($select);
		$this->db->from($table);
		if(!empty($condition)){ 
			$this->db->where($condition);
		}	
		if(!empty($order_by) && !empty($order)){ 
			$this->db->order_by($order_by, $order);
		}
		$query = $this->db->get();
		// echo "<pre>";
		// print_r($query);
		// exit();
		if ($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return false;
		}
	}

	function getreturndata($estimate_id)
	{
	$this->db->select('tp.product_name,tl.location_name,edq.required_qty,edq.remaining_qty,edq.estimate_product_detail_qty_id,edq.prev_qty');
    $this->db->from('tbl_estimate_product_detail_qty edq'); 
    $this->db->join('tbl_location tl', 'tl.location_id=edq.location_id', 'left');
    $this->db->join('tbl_product tp', 'tp.product_id=edq.product_id', 'left');
    // $condition="edq.estimate_id='".$estimate_id."' and edq.required_qty >,1";
    // $this->db->where($condition);
    $condition="edq.estimate_id= '".$estimate_id."' and  edq.remaining_qty > '". 0 ."'";
    // $this->db->where('edq.estimate_id',$estimate_id);
     $this->db->where("$condition");

    // $this->db->order_by('c.track_title','asc');         
    $query = $this->db->get(); 
    if($query->num_rows() != 0)
    {
        return $query->result_array();
    }
    else
    {
        return false;
    }

	}

	function getbelongchallan()
	{
		$this -> db -> select('i.*, c.company_name, u.first_name, u.last_name,tcpl.pick_location_id,lo.location_name as challan_start_location, lo1.location_name as challan_end_location');
				$this -> db -> from("tbl_challan as i");
		$this -> db -> join("tbl_company as c", "c.company_id = i.company_id", "");
		$this -> db -> join("tbl_challan_pickup_location as tcpl","tcpl.challan_id= i.challan_id","");
		$this -> db -> join("tbl_location_users AS tlu","tlu.location_id = i.start_location OR tlu.location_id = i.end_location OR tlu.location_id = tcpl.pick_location_id","");
		$this->db->join("tbl_location as lo ","lo.location_id=i.start_location ");
		$this->db->join("tbl_location as lo1 ","lo1.location_id=i.end_location");
		$this -> db -> join("tbl_users as u", "u.user_id = tlu.user_id", "");
		$this->db->where("u.user_id = '".$_SESSION['jain_decorator_admin'][0]['user_id']."' OR i.created_by='".$_SESSION['jain_decorator_admin'][0]['user_id']."' ");
		$this->db->order_by('i.challan_id','desc');
		$this->db->group_by("i.challan_id");
		$query = $this -> db -> get();
		if ($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return false;
		}
		// echo "<pre>";
		// print_r($query);
		// exit();
	}	

	function getbelongpreparedchallan()
	{
		$this -> db -> select('i.*, c.company_name,tcpl.pick_location_id,lo.location_name as challan_start_location, lo1.location_name as challan_end_location');
				$this -> db -> from("tbl_challan as i");
		$this -> db -> join("tbl_company as c", "c.company_id = i.company_id", "left");
		$this -> db -> join("tbl_challan_pickup_location as tcpl","tcpl.challan_id= i.challan_id","left");
		$this -> db -> join("tbl_location_users AS tlu","tlu.location_id = i.start_location OR tlu.location_id = i.end_location OR tlu.location_id = tcpl.pick_location_id","left");
		$this->db->join("tbl_location as lo ","lo.location_id=i.start_location ","left");
		$this->db->join("tbl_location as lo1 ","lo1.location_id=i.end_location","left");
		// $this -> db -> join("tbl_users as u", "u.user_id = tlu.user_id", "");
		$this->db->where("i.created_by='".$_SESSION['jain_decorator_admin'][0]['user_id']."' AND i.status ='Prepared' ");
		$this->db->order_by('i.challan_id','desc');
		$this->db->group_by("i.challan_id");
		$query = $this -> db -> get();
		if ($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return false;
		}
		// echo "<pre>";
		// print_r($query);
		// exit();
	}	

	function getbelongreceivablechallan()
	{
			$getUserlocations = $this->challanmodel->getdata("tbl_location_users", "location_id", "user_id='" .$_SESSION['jain_decorator_admin'][0]['user_id']. "' ");
			//echo "<pre>";print_r($getUserlocations);exit;
			$user_locations = array();
			if(!empty($getUserlocations)){
				for($i=0; $i < sizeof($getUserlocations);$i++){
					$user_locations[] = $getUserlocations[$i]['location_id'];
				}
				
				$import_str = implode(",",$user_locations);
			}

		// $this -> db -> select('i.*, c.company_name,tcpl.pick_location_id,lo.location_name as challan_start_location, lo1.location_name as challan_end_location');
		// 		$this -> db -> from("tbl_challan as i");
		// $this -> db -> join("tbl_company as c", "c.company_id = i.company_id", "left");
		// $this -> db -> join("tbl_challan_pickup_location as tcpl","tcpl.challan_id= i.challan_id","left");
		// $this -> db -> join("tbl_location_users AS tlu","tlu.location_id = i.start_location OR tlu.location_id = i.end_location OR tlu.location_id = tcpl.pick_location_id","left");
		// $this->db->join("tbl_location as lo ","lo.location_id=i.start_location ","left");
		// $this->db->join("tbl_location as lo1 ","lo1.location_id=i.end_location","left");
		// $this -> db -> join("tbl_users as u", "u.user_id = tlu.user_id", "");
		// $this->db->where("(i.approved_by ='".$_SESSION['jain_decorator_admin'][0]['user_id']."' OR ) AND i.status ='Prepared'  ");

		$this -> db -> select('i.*, c.company_name,tcpl.pick_location_id,lo.location_name as challan_start_location, lo1.location_name as challan_end_location');
			$this -> db -> from("tbl_challan as i");
			$this -> db -> join("tbl_company as c", "c.company_id = i.company_id", "left");
			$this -> db -> join("tbl_challan_pickup_location as tcpl","tcpl.challan_id= i.challan_id","left");

			$this -> db -> join("tbl_challan_locations_products_qty as cl ", "cl.challan_id=i.challan_id","left");
			// $this -> db -> join("tbl_challan_locations_products_qty as tclpq ", "tclpq.challan_id=i.challan_id","");
			$this -> db -> join("tbl_product as p ", "p.product_id=cl.product_id","left");
			
			$this->db->join("tbl_location as lo ","lo.location_id=i.start_location ","left");
			$this->db->join("tbl_location as lo1 ","lo1.location_id=i.end_location","left");
		// }
			
		$this->db->where("(i.approved_by = '".$_SESSION['jain_decorator_admin'][0]['user_id']."' OR tcpl.pick_location_id IN (".$import_str.") OR i.start_location IN (".$import_str.") ) AND i.status = 'Prepared'");

		$this->db->order_by('i.challan_id','desc');
		$this->db->group_by("i.challan_id");
		$query = $this -> db -> get();
		if ($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return false;
		}
		// echo "<pre>";
		// print_r($query);
		// exit();
	}

	function getDataLimit($table,$select = "*", $condition = '', $order_by = "",$order = "", $limit){
		$this->db->select($select);
		$this->db->from($table);
		if(!empty($condition)){ 
			$this->db->where($condition);
		}	
		if(!empty($order_by) && !empty($order)){ 
			$this->db->order_by($order_by, $order);
		}
		if(!empty($limit) && !empty($limit)){ 
			$this->db->limit($limit);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return false;
		}
	}

	/*
		 * Fetch All records from single table with multiple condition, Sorting, Limit, Group By
	*/
	function Fetch($table, $columns, $condition = null, $sort_by = null, $group_by = null, $limit = null) {
		// print_r($limit);
		// exit;
		if (is_array($columns)) {
			$columns = implode(", ", $columns);
		}

		if (is_null($condition) || $condition == "") {
			$condition = "1=1";
		}

		$sort_order = "";
		if (is_array($sort_by) && $sort_by != null) {
			foreach ($sort_by as $key => $val) {
				$sort_order .= ($sort_order == "") ? "order by $key $val" : ", $key $val";
			}
		}

		if ($group_by != null) {
			$group_by = "group by " . $group_by;
		}

		$limit_by = "";
		if(is_array($limit) && $limit["offset"] != null && $limit["rows"] != null) {
			$limit_by = "limit ".$limit["offset"].", ".$limit["rows"];
		}
		// echo $limit;exit;

		$sql = trim("select $columns from $table where $condition $group_by $sort_order $limit_by");
		$query = $this->db->query($sql);
		return $query;
	}

	/*
		 * Fetch records from multiple tables [Join Queries] with multiple condition, Sorting, Limit, Group By
	*/
	function JoinFetch($main_table = array(), $join_tables = array(), $condition = null, $sort_by = null, $group_by = null) {
		$columns = isset($main_table[1]) ? $main_table[1] : array();
		$main_table = $main_table[0];
		$join_str = "";
		foreach ($join_tables as $join_table) {
			$join_str .= $join_table[0] . " join " . $join_table[1] . " on (" . $join_table[2] . ") ";
			if (isset($join_table[3])) {
				$columns = array_merge($columns, $join_table[3]);
			}
		}
		$columns = (sizeof($columns) > 0) ? implode(", ", $columns) : "*";
		if (is_null($condition) || $condition == "") {
			$condition = "1=1";
		}
		$sort_order = "";
		if (is_array($sort_by) && $sort_by != null) {
			foreach ($sort_by as $key => $val) {
				$sort_order .= ($sort_order == "") ? "order by $key $val" : ", $key $val";
			}
		}
		if ($group_by != null) {
			$group_by = "group by " . $group_by;
		}
		$sql = trim("select $columns from $main_table $join_str where $condition $group_by $sort_order");
		$query = $this->db->query($sql);
		return $query;
	}

	/*
		 * Fetch as per the request like [Array,Object,Asscociative Array]
	*/
	function MySqlFetchRow($result, $type = 'assoc') {
		$row = false;
		if ($result != false) {
			switch ($type) {
			case 'array':
				$row = @$result->result_array();
				break;
			case 'object':
				$row = @$result->result();
				break;
			default:
			case 'assoc':
				$row = @$result->row_array();
				break;
			}
		}
		return $row;
	}
	
	function insertData($tbl_name, $data_array) {
		$result_id = "";
		$this->db->insert($tbl_name, $data_array);
		$result_id = $this->db->insert_id();
		if($result_id > 0) {
			return $result_id;
		}else{
			return false;
		}
	}

	function updateData($tbl_name, $data_array, $condition) {
		$this->db->where($condition);
		if ($this->db->update($tbl_name, $data_array)) {
			return true;
		} else {
			return false;
		}
	}
	
	function deleteRecord($tbl_name,$condition){
		$this->db->where($condition);
		if($this->db->delete($tbl_name)){
			return true;
		}else{
			return false;
		}
	}
		
	function getEmailContent($mail_key){	
		$this->db->select('*');
		$this->db->from('tbl_emailcontents');
		$this->db->where("mail_Key", $mail_key);
		$query = $this->db->get();
		if($query -> num_rows() > 0){
			$res = $query->result_array();
			return $res[0];
		}else{
			return false;
		}
	}
	
	
	
	//ravi new code start
	
	
		function getDataravi($table, $select = "*", $condition = array()) {
		$this->db->select($select);
		$this->db->from($table);
		$this->db->where($condition);
		$query = $this->db->get();
		if($query->num_rows() >= 1) {
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	
	
	
	
	//ravi new code end
	
	
	
	
}
?>