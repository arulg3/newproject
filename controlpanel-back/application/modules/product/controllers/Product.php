<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Product extends CI_Controller 
{	
	function __construct(){
		parent::__construct();
		$this->load->model('productmodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
		checklogin();
	}
 
	function index(){

		$data = array();
		$categories_sql = $this->common->Fetch("tbl_category", "category_id,category_name", "status = 'Active' ", array("category_name" => "ASC"));
		$data['categories'] = $this->common->MySqlFetchRow($categories_sql, "array");

		$this->load->view('template/header.php');
		$this->load->view('product/index',$data );
		$this->load->view('template/footer.php');
	}

	function getProduct(){

		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			// $getProduct = $this->common->Fetch("tbl_product","*","status='Active' AND
				 // category_id =".$_POST['category_id']." ",array("category_id"=>'DESC'));
			$getProduct = $this->common->getData("tbl_product","*","status='Active' AND
				 category_id =".$_POST['category_id']." " );
			$option="";
			
			if($getProduct){
				$option .='<option value="">All Product</option>';
				foreach ($getProduct as $key => $value) {
					$option .='<option value='.$value['product_id'].'>'.$value['product_name'].'</option>';
				}
			}else{
					$option .='<option value="">All Product</option>';
			}
				echo json_encode(array('success'=>true, 'option'=>$option));
				exit;
		}
	}
	function addEdit(){
		$record_id = "";
		$data = array();

		// $sql_categories = $this->common->Fetch("tbl_category","category_id,category_name","status='Active'",array("category_name"=>"ASC"));
		$sql_categories = $this->common->Fetch("tbl_category","category_id,category_name","",array("category_name"=>"ASC"));
		$data['categories'] = $this->common->MySqlFetchRow($sql_categories,"array");

		$sql_locations = $this->common->Fetch("tbl_location","location_id,location_name,location_code","status='Active'",array("location_name"=>"ASC"));
		$data['locations'] = $this->common->MySqlFetchRow($sql_locations,"array");

		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$record_id = $url_prams['id'];
			$result = $this->common->getData("tbl_product","*",array("product_id"=>$record_id));
			if(!empty($result)){
				$data['category_data'] = $result[0];

				$sql_product_locations = $this->common->Fetch("tbl_product_qty_location_details","*","product_id='".$record_id."'");
				$data['product_locations'] = $this->common->MySqlFetchRow($sql_product_locations,"array");

				// $sql_product_locations = $this->common->Fetch("tbl_product_current_qty","*","product_id='".$record_id."'");
				// $data['product_currentqty_locations'] = $this->common->MySqlFetchRow($sql_product_locations,"array");
				// ECHO "<pre>";
				// print_r($data['product_currentqty_locations']);
				// print_r($data['product_locations']);
				// print_r(array_push($data['product_currentqty_locations'], var)(,$data['product_locations']));
			
				// exit();
			}
		}

		// echo "<pre>";
		// print_r($data);
		// exit;

		$this->load->view('template/header.php');
		$this->load->view('product/addEdit',$data);
		$this->load->view('template/footer.php');
	}

	function fetch(){
		$get_result = $this->productmodel->getRecords($_GET);
		$result = array();
		$result["sEcho"]= $_GET['sEcho'];
		$result["iTotalRecords"] = $get_result['totalRecords'];	//iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"]= $get_result['totalRecords']; //iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result']) && count($get_result['query_result']) > 0){
			for($i=0;$i<sizeof($get_result['query_result']);$i++){
				$temp = array();
				$status_change = "";
				$actionCol = "";
				$status_type = '';
				if($get_result['query_result'][$i]->status == 'Active'){
					$status_type = "checked = 'checked'";
				}
				
				$status_change = '<div class="switch">
									<label>
										<input type="checkbox" class="status_change" onchange="changeStatus('.$get_result['query_result'][$i]->product_id.',this)"  '.$status_type.'">
										<span class="lever"></span>
									</label>
								</div>';
				$actionCol .='<a href="product/addEdit?text='.rtrim(strtr(base64_encode("id=".$get_result['query_result'][$i]->product_id), '+/', '-_'), '=').'" title="Edit" class="text-center"><i class="material-icons ">create</i></a>';
				

				$totalproqty=  $this->common->getData("tbl_product_current_qty","sum(product_qty) as currentqty",array("product_id"=>$get_result['query_result'][$i]->product_id));

				$image = "No Image";
				if(!empty($get_result['query_result'][$i]->product_image)){					
					$image = "<img src='".base_url("uploads/product_images/".$get_result['query_result'][$i]->product_image)."' class='preview_uploaded_image'>";
				}
				array_push($temp, $get_result['query_result'][$i]->category_name);
				array_push($temp, $get_result['query_result'][$i]->product_name);
				array_push($temp, $get_result['query_result'][$i]->product_size);
				array_push($temp,$totalproqty[0]['currentqty']);
				
				array_push($temp,$image);

				array_push($temp, $get_result['query_result'][$i]->status);
				array_push($temp, $status_change);
				array_push($temp, $actionCol);
				array_push($items, $temp);
			}
		}
		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}
	
	function submitForm()
	{
		// echo "<pre>";
		// print_r($_POST);
		// exit;

		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			/*check duplicate entry*/	
			$condition = "product_name = '".$this->input->post('product_name')."' ";
			if(!empty($this->input->post("product_id"))){
				$condition .= " AND product_id <> ".$this->input->post("product_id");
			}	

			$chk_product_sql = $this->common->Fetch("tbl_product","product_id",$condition);
			$rs_product = $this->common->MySqlFetchRow($chk_product_sql, "array");

			if(!empty($rs_product[0]['product_id'])){
				echo json_encode(array('success'=>false, 'msg'=>'Product name already exist...'));
				exit;
			}
			// product image

			if(!empty($_FILES['product_image'])){
				// print_r($_FILES);
				// exit();
				$path = DOC_ROOT_FRONT.'/uploads/product_images/';
			
				if(!empty($this->input->post('existing_product_image'))){
					$img_path = $path."/".$this->input->post('existing_product_image');
					if(is_file($img_path)){
						unlink($img_path);
					}
				}
				$this->load->library('upload');
				$this->upload->initialize($this->set_upload_options());
                if (!$this->upload->do_upload('product_image')){
					$error = array('error' => $this->upload->display_errors());
					echo json_encode(array('success'=>false, 'msg'=>$error));
					exit;
                }else{
					$file_data = array('upload_data' => $this->upload->data());
					$data['product_image'] = $file_data['upload_data']['file_name'];
                }
			}
			//

			// $data = array();
			$data['category_id'] = $this->input->post('category_id');
			$data['product_name'] = $this->input->post('product_name');
			$data['product_size'] = $this->input->post('product_size');

			// $data['phone_number'] = $this->input->post('phone_number');

			if(!empty($this->input->post("product_id"))){

				$data['updated_by'] = $_SESSION['jain_decorator_admin'][0]['user_id'];
				$data['updated_on'] = date("Y-m-d H:i:s");
				$data['remark']=$this->input->post('remark');

				$result = $this->common->updateData("tbl_product",$data,array("product_id"=>$this->input->post("product_id")));
				if($result){

					// $this->common->deleteRecord("tbl_product_qty_location_details","product_id = '".$this->input->post("product_id")."' ");

					// if(!empty($_POST['product_qty']) && count($_POST['product_qty']) > 0){
					// 	foreach ($_POST['product_qty'] as $key => $value) {
							
					// 		$prod_qty_data = array();
					// 		$prod_qty_data['product_id'] = $this->input->post("product_id");
					// 		$prod_qty_data['quantity'] = $value;
					// 		$prod_qty_data['location_id'] = $_POST['product_location'][$key];

					// 		$this->common->insertData("tbl_product_qty_location_details",$prod_qty_data,"1");
					// 	}
					// }

					if(!empty($_POST['product_qty']) && count($_POST['product_qty']) > 0){
						foreach ($_POST['product_qty'] as $key => $value) {
							
							$prod_qty_data = array();
							$prod_qty_data['status'] = $_POST['status'][$key];
							
							$prod_curr_qty_data = array();
							// $prod_curr_qty_data['product_id'] = $this->input->post("product_id");
							// $prod_curr_qty_data['location_id'] = $_POST['product_location'][$key];
							// $prod_curr_qty_data['product_qty'] = $value;
							$prod_curr_qty_data['updated_by'] = $_SESSION['jain_decorator_admin'][0]['user_id'];
							$prod_curr_qty_data['updated_on'] = date("Y-m-d H:i:s");
							
							$prod_qty_log_data = array();
							$prod_qty_log_data['product_id'] = $this->input->post("product_id");
							$prod_qty_log_data['from_location_id'] = $_POST['product_location'][$key];
							$prod_qty_log_data['to_location_id'] = $_POST['product_location'][$key];

							if(!empty($_POST['product_qty_location_id'][$key])){

								$existing_loc_qty_data = $this->common->getData("tbl_product_qty_location_details","*",array('product_qty_location_id'=>$_POST['product_qty_location_id'][$key]));
								
								if(!empty($existing_loc_qty_data)){

									if($value != $existing_loc_qty_data[0]['product_new_quantity']){

										$existing_curr_loc_qty_data = $this->common->getData("tbl_product_current_qty","*",array('product_id' => $this->input->post("product_id"), "location_id" => $_POST['product_location'][$key] ));
										
										if(!empty($existing_curr_loc_qty_data))
										{
											$qty_difference_from_curr = 0;

											if($value > $existing_loc_qty_data[0]['product_new_quantity']){
												//qty credited
												$qty_difference_from_curr = $value - $existing_loc_qty_data[0]['product_new_quantity'];

												$prod_curr_qty_data['product_qty'] = $existing_curr_loc_qty_data[0]['product_qty'] + $qty_difference_from_curr;

												$prod_qty_log_data['credit_or_debit'] = "Credit";		/*'Credit','Debit'*/

											}elseif($value < $existing_loc_qty_data[0]['product_new_quantity']) {
												//qty debited
												$qty_difference_from_curr =  $existing_loc_qty_data[0]['product_new_quantity'] - $value;

												$prod_curr_qty_data['product_qty'] = $existing_curr_loc_qty_data[0]['product_qty'] - $qty_difference_from_curr;

												$prod_qty_log_data['credit_or_debit'] = "Debit";		/*'Credit','Debit'*/
											}


											$this->common->updateData("tbl_product_current_qty",$prod_curr_qty_data,array("product_id" => $this->input->post("product_id"), "location_id" => $_POST['product_location'][$key]));

											$prod_qty_log_data['quantity'] = $qty_difference_from_curr;
											$prod_qty_log_data['log_action'] = "Edit";  /*Add,Edit,Challan,Estimate,Return*/
											$prod_qty_log_data['created_by'] = $_SESSION['jain_decorator_admin'][0]['user_id'];
											$prod_qty_log_data['created_on'] = date("Y-m-d H:i:s");
											$prod_qty_log_data['updated_by'] = $_SESSION['jain_decorator_admin'][0]['user_id'];
											$prod_qty_log_data['updated_on'] = date("Y-m-d H:i:s");
											$this->common->insertData("tbl_product_qty_log",$prod_qty_log_data,"1");

										}	
										
										$prod_qty_data['product_new_quantity'] = $value;

									}
									
									$prod_qty_data['updated_by'] = $_SESSION['jain_decorator_admin'][0]['user_id'];
									$prod_qty_data['updated_on'] = date("Y-m-d H:i:s");

									$this->common->updateData("tbl_product_qty_location_details",$prod_qty_data,array("product_qty_location_id"=>$_POST['product_qty_location_id'][$key]));


								}

							}else{

								/*product location and quantity data*/
								$prod_qty_data['product_id'] = $this->input->post("product_id");
								$prod_qty_data['location_id'] = $_POST['product_location'][$key];
								$prod_qty_data['product_old_quantity'] = $value;
								$prod_qty_data['product_new_quantity'] = $value;
								$prod_qty_data['created_by'] = $_SESSION['jain_decorator_admin'][0]['user_id'];
								$prod_qty_data['created_on'] = date("Y-m-d H:i:s");
								
								$this->common->insertData("tbl_product_qty_location_details",$prod_qty_data,"1");
								
								/*product current quantity data*/
								// $prod_curr_qty_data = array();
								$prod_curr_qty_data['product_id'] = $this->input->post("product_id");
								$prod_curr_qty_data['location_id'] = $_POST['product_location'][$key];
								$prod_curr_qty_data['product_qty'] = $value;
								$prod_curr_qty_data['created_by'] = $_SESSION['jain_decorator_admin'][0]['user_id'];
								$prod_curr_qty_data['created_on'] = date("Y-m-d H:i:s");
								
								$this->common->insertData("tbl_product_current_qty",$prod_curr_qty_data,"1");

								/*product quantity log data*/
								// $prod_qty_log_data = array();
								// $prod_qty_log_data['product_id'] = $result;
								// $prod_qty_log_data['from_location_id'] = $_POST['product_location'][$key];
								// $prod_qty_log_data['to_location_id'] = $_POST['product_location'][$key];
								$prod_qty_log_data['credit_or_debit'] = "Credit";		/*'Credit','Debit'*/
								$prod_qty_log_data['quantity'] = $value;
								$prod_qty_log_data['log_action'] = "Add";  /*Add,Edit,Challan,Estimate,Return*/
								$prod_qty_log_data['created_by'] = $_SESSION['jain_decorator_admin'][0]['user_id'];
								$prod_qty_log_data['created_on'] = date("Y-m-d H:i:s");
								$prod_qty_log_data['updated_by'] = $_SESSION['jain_decorator_admin'][0]['user_id'];
								$prod_qty_log_data['updated_on'] = date("Y-m-d H:i:s");

								$this->common->insertData("tbl_product_qty_log",$prod_qty_log_data,"1");
							}
						}
					}
					
					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while Updating data.'));
					exit;
				}
			}else{

				$data['created_by'] = $_SESSION['jain_decorator_admin'][0]['user_id'];
				$data['created_on'] = date("Y-m-d H:i:s");

				// print_r($data);
				// exit();
				$result = $this->common->insertData("tbl_product",$data,"1");
				if($result){

					if(!empty($_POST['product_qty']) && count($_POST['product_qty']) > 0){
						foreach ($_POST['product_qty'] as $key => $value) {
							
							/*product location and quantity data*/
							$prod_qty_data = array();
							$prod_qty_data['product_id'] = $result;
							$prod_qty_data['location_id'] = $_POST['product_location'][$key];
							$prod_qty_data['product_old_quantity'] = $value;
							$prod_qty_data['product_new_quantity'] = $value;
							$prod_qty_data['status'] = $_POST['status'][$key];
							$prod_qty_data['created_by'] = $_SESSION['jain_decorator_admin'][0]['user_id'];
							$prod_qty_data['created_on'] = date("Y-m-d H:i:s");

							$this->common->insertData("tbl_product_qty_location_details",$prod_qty_data,"1");

							/*product current quantity data*/
							$prod_curr_qty_data = array();
							$prod_curr_qty_data['product_id'] = $result;
							$prod_curr_qty_data['location_id'] = $_POST['product_location'][$key];
							$prod_curr_qty_data['product_qty'] = $value;
							$prod_curr_qty_data['created_by'] = $_SESSION['jain_decorator_admin'][0]['user_id'];
							$prod_curr_qty_data['created_on'] = date("Y-m-d H:i:s");
							$prod_curr_qty_data['updated_by'] = $_SESSION['jain_decorator_admin'][0]['user_id'];
							$prod_curr_qty_data['updated_on'] = date("Y-m-d H:i:s");

							$this->common->insertData("tbl_product_current_qty",$prod_curr_qty_data,"1");

							/*product quantity log data*/
							$prod_qty_log_data = array();
							$prod_qty_log_data['product_id'] = $result;
							$prod_qty_log_data['from_location_id'] = $_POST['product_location'][$key];
							$prod_qty_log_data['to_location_id'] = $_POST['product_location'][$key];
							$prod_qty_log_data['credit_or_debit'] = "Credit";		/*'Credit','Debit'*/
							$prod_qty_log_data['quantity'] = $value;
							$prod_qty_log_data['log_action'] = "Add";  /*Add,Edit,Challan,Estimate,Return*/
							$prod_qty_log_data['created_by'] = $_SESSION['jain_decorator_admin'][0]['user_id'];
							$prod_qty_log_data['created_on'] = date("Y-m-d H:i:s");
							$prod_qty_log_data['updated_by'] = $_SESSION['jain_decorator_admin'][0]['user_id'];
							$prod_qty_log_data['updated_on'] = date("Y-m-d H:i:s");

							$this->common->insertData("tbl_product_qty_log",$prod_qty_log_data,"1");
						}
					}

					echo json_encode(array('success'=>true, 'msg'=>'Record Inserted Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while Inserting data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem while add/edit data.'));
			exit;
		}
	}

	function set_upload_options($file_name = ""){   
		$config = array();
		if(!empty($file_name) &&  $file_name != ""){
			$config['file_name']   = $file_name;
		}
		$config['upload_path'] = DOC_ROOT_FRONT ."/uploads/product_images/";
		if(!is_dir($config['upload_path'])){
			mkdir($config['upload_path'],0777, true);		
		}
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size']      = '0';
		$config['overwrite']     = FALSE;
		return $config;
	} 
	
	function changeStatus(){
		if(!empty($this->input->post("id"))){
			$existing_data = $this->common->getData("tbl_product","status",array('product_id'=>$this->input->post("id")));
			$data = array();
			if(!empty($existing_data) && $existing_data[0]['status'] == "Active"){
				$data['status'] = "In-active";
			}else{
				$data['status'] = "Active";
			}
			$result = $this->common->updateData("tbl_product",$data,array("product_id"=>$this->input->post("id")));
			if($result){
				echo json_encode(array('success'=>true));
				exit;
			}else{
				echo json_encode(array('success'=>false));
			}
		}
	}


	function deleteFile(){
		$path = DOC_ROOT_FRONT.'/uploads/product_images/';
		if(!empty($this->input->post('name'))){
			$data = array();
			$img_path = $path."/".$this->input->post('name');
			if(is_file($img_path)){
				unlink($img_path);
			}
			$data['product_image'] = "";
			$result = $this->common->updateData("tbl_product",$data,array("product_id"=>$this->input->post("id")));
			if($result){
				echo json_encode(array('success'=>true));
				exit;
			}else{
				echo json_encode(array('success'=>false));
			}
		}
	}
}

?>
