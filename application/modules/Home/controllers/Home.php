<?php
if (!defined('BASEPATH'))
exit('No direct script access allowed');
class Home extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('homemodel', '', TRUE);
        $this->config->set_item('language', $this->session->userdata('current_language'));
        $this->session->set_userdata('site_lang', "french");
    }
    function index()
    {
        $this->load->view('templates/header.php');
        $this->load->view('Home/index');
        $this->load->view('templates/footer.php');
    }
}
?>