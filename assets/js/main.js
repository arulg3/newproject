function openNav() {
    document.getElementById("mySidenav").style.width = "100%";
}
  
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
jQuery(document).ready(function($){
    AOS.init({
        duration: 600,
    })
    $(".mainmenu-area").sticky({topSpacing:0});
    $('.related-products-carousel').owlCarousel({
        loop:true,
        nav:true,
        margin:20,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
            },
            600:{
                items:1,
            },
            1000:{
                items:1,
            },
            1200:{
                items:1,
            }
        }
    });  
    
    $('.brand-list').owlCarousel({
        loop:true,
        nav:true,
        margin:20,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
            },
            600:{
                items:1,
            },
            1000:{
                items:1,
            }
        }
    });    
    
    
    // Bootstrap Mobile Menu fix
  $(".navbar-nav li a").click(function(){
      $(".navbar-collapse").removeClass('in');
  });    
  
  // jQuery Scroll effect
  $('.navbar-nav li a, .scroll-to-up').bind('click', function(event) {
      var $anchor = $(this);
      var headerH = $('.header-area').outerHeight();
      $('html, body').stop().animate({
          scrollTop : $($anchor.attr('href')).offset().top - headerH + "px"
      }, 1200, 'easeInOutExpo');

      event.preventDefault();
  });    
  
  // Bootstrap ScrollPSY
  $('body').scrollspy({ 
      target: '.navbar-collapse',
      offset: 95
  })      
});

$(document).ready(function(){
    $('#login-trigger').click(function() {
        $(this).next('#login-content').slideToggle();
        $(this).toggleClass('active');                    
        if ($(this).hasClass('active')){
            $(this).find('span').html('&#x25B2;')
        }else{ 
            $(this).find('span').html('&#x25BC;')
        }
    })
});