<script src="http://malsup.github.com/jquery.form.js"></script> 
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

<style>

.content 
{
    // margin-bottom: 0;
    // margin-left: 12.422% !important;
    // margin-right: 0;
    // margin-top: 0;
    padding: 28px;
	height: 575px;
}
 
input
{
	margin:auto;
}

.content
{
	width: 50%;
    margin: auto !important;
}

</style>
<div class="container-fluid-full">
	<div class="row-fluid">					
		<div class="row-fluid">				
			<!-- start: Content -->
			<div id="" class="content span10">
				<!--<ul class="breadcrumb">
					<li>
						<i class="icon-home"></i>
						<a href="<?php echo base_url();?>login">Back To Login</a> 
						<i class="icon-angle-right"></i>
					</li>
					<li><a href="<?php echo base_url();?>forgetchangepassword">Forgot Password</a></li>
				</ul>-->
				<div class="card row-fluid sortable" id="change_password_div">
					<div class="box span12">
						<div class="box-header" style="text-align:center;" data-original-title>
							<h2><i class="halflings-icon edit"></i><span class="break"></span>Change Password</h2>
						</div>
						
						<div class="box-content"  style="text-align:center;" >
							<div id="showmsg"></div>
							<form class="form-horizontal" id="form-validate" method="post">
								<fieldset>
									<input type="hidden" id="user_id" name="user_id" value="<?php if(!empty($user_id)){echo $user_id;}?>" />	
									<input type="hidden" id="email_id" name="email_id" value="<?php if(!empty($email_id)){echo $email_id;}?>" />
									
									<div class="control-group">
										<label class="control-label" for="newpassword">New Password*</label>
										<div class="controls">
											<input class="input-xlarge form-control" id="newpassword" name="newpassword" type="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"  autocomplete="off">
										</div>
										<div id="message">
										  <label>Password must contain the following:</label>
										  <p id="letter" class="invalid">A <b>lowercase</b> letter</p>
										  <p id="capital" class="invalid">A <b>capital (uppercase)</b> letter</p>
										  <p id="number" class="invalid">A <b>number</b></p>
										  <p id="length" class="invalid">Minimum <b>8 characters</b></p>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label" for="confirmpassword">Re-Confirm Password*</label>
										<div class="controls">
											<input class="input-xlarge form-control" id="confirmpassword" name="confirmpassword" type="password">
										</div>
									</div>
									
									<div class="clearfix" style="height: 10px; width: 100%; float: left; display: inline;">&nbsp;</div>
									
									<div class="form-actions">
										<button type="submit" class="btn btn-primary">Submit</button>
										<!--<a href="<?php echo base_url();?>home"><button class="btn" type="button">Cancel</button></a>-->
									</div>
									
									<div class="clearfix" style="height: 10px; width: 100%; float: left; display: inline;">&nbsp;</div>
									
									<div class="form-group mt-20">
										<p class="semibold-text mb-0"><a href="<?php echo base_url();?>login"><i class="fa fa-angle-left fa-fw"></i> Back to Login</a></p>
									</div>
									
								</fieldset>
							</form>
						</div>
					</div><!--/span-->
				</div><!--/row-->
			</div><!-- end: Content -->			
		</div>
	</div>
</div>
<style>
/* Style all input fields */
/* Style the submit button */
#change_password_div input[type=submit] {background-color: #4CAF50;color: white;}

/* The message box is shown when the user clicks on the password field */
#change_password_div  #message {display:none;color: #000;position: relative;padding: 20px;margin-top: 5px;text-align:left;}
#change_password_div #message p {padding: 10px 20px;font-size: 14px;margin:0px !important;}

/* Add a green text color and a checkmark when the requirements are right */
#change_password_div .valid {color: green;}

#change_password_div .valid:before {position: relative;left: -35px;content: "✔";}

/* Add a red text color and an "x" when the requirements are wrong */
#change_password_div .invalid {color: red;}
#change_password_div .invalid:before {position: relative;left: -35px;content: "✖";}

</style>
<script>
$(document).ready(function(){
	var myInput = document.getElementById("newpassword");
	var letter = document.getElementById("letter");
	var capital = document.getElementById("capital");
	var number = document.getElementById("number");
	var length = document.getElementById("length");

	// When the user clicks on the password field, show the message box
	myInput.onfocus = function() {
		document.getElementById("message").style.display = "block";
	}

	// When the user clicks outside of the password field, hide the message box
	myInput.onblur = function() {
		document.getElementById("message").style.display = "none";
	}

		// When the user starts to type something inside the password field
	myInput.onkeyup = function() {
		// Validate lowercase letters
		var lowerCaseLetters = /[a-z]/g;
		if(myInput.value.match(lowerCaseLetters)) {  
			letter.classList.remove("invalid");
			letter.classList.add("valid");
		} else {
			letter.classList.remove("valid");
			letter.classList.add("invalid");
		}
	  
		// Validate capital letters
		var upperCaseLetters = /[A-Z]/g;
		if(myInput.value.match(upperCaseLetters)) {  
			capital.classList.remove("invalid");
			capital.classList.add("valid");
		} else {
			capital.classList.remove("valid");
			capital.classList.add("invalid");
		}

		// Validate numbers
		var numbers = /[0-9]/g;
		if(myInput.value.match(numbers)) {  
			number.classList.remove("invalid");
			number.classList.add("valid");
		} else {
			number.classList.remove("valid");
			number.classList.add("invalid");
		}
	  
	  // Validate length
		if(myInput.value.length >= 8) {
			length.classList.remove("invalid");
			length.classList.add("valid");
		} else {
			length.classList.remove("valid");
			length.classList.add("invalid");
		}
	}

});
</script>

<script>
var vRules = {
	newpassword:{required:true}, 
	confirmpassword:{required:true,equalTo:"#newpassword" }
};

var vMessages = {
	newpassword:{required:"Please enter New Password."},
	confirmpassword:{required:"Please confirm your password.", equalTo:"Password doesn't match" }
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>forgetchangepassword/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'POST',
			dataType: 'JSON',
			cache: false,
			clearForm: false,
			success: function (response)
			{
				if(response.success)
				{					
					$('#showmsg').html('<span style="color:#339900;">'+response.msg+'</span>');
					setTimeout(function(){
						window.location = "<?php echo base_url();?>login";
					},2000);
				}
				else
				{
					$('#showmsg').html('<span style="color:#ff0000;">'+response.msg+'</span>');
					return false;
				}
			}
		});
	}
});


</script>