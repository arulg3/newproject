<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD</h2>
            </div>
            <!-- Widgets -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					
					<div class="body">
						<ol class="breadcrumb align-right">
							<li><a href="<?php echo base_url("home");?>"><i class="material-icons">home</i> Home</a></li>
							<li><a href="<?php echo base_url($this->router->fetch_module());?>"><i class="material-icons">person</i>Users</a></li>
							<li class="active"><i class="material-icons">mode_edit</i> Add Edit</li>
						</ol>
					</div>
                    <div class="card">
                        <div class="header">
                            <h2>Add / Edit Users</h2>
                        </div>
                        <div class="body">
                            <form id="form-validate" class="form-validate" name="form-validate" method="POST">
								<input type="hidden" name="user_id" id="user_id" value="<?php echo(!empty($users_data['user_id']))?$users_data['user_id']:"";?>">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="first_name" id="first_name" value="<?php echo(!empty($users_data['first_name']))?$users_data['first_name']:"";?>" required>
                                        <label class="form-label">First Name</label>
                                    </div>
                                    <div class="help-info"></div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="last_name" id="last_name" value="<?php echo(!empty($users_data['last_name']))?$users_data['last_name']:"";?>" required>
                                        <label class="form-label">Last Name</label>
                                    </div>
                                    <div class="help-info"></div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="email" class="form-control" name="email_id" id="email_id" value="<?php echo(!empty($users_data['email_id']))?$users_data['email_id']:"";?>" required>
                                        <label class="form-label">Email Id</label>
                                    </div>
                                    <div class="help-info"></div>
                                </div>
								<div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="phone_number" id="phone_number" value="<?php echo(!empty($users_data['phone_number']))?$users_data['phone_number']:"";?>" maxlength="10" required>
                                        <label class="form-label">Phone Number</label>
                                    </div>
                                    <div class="help-info"></div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <textarea class="form-control" name="address" id="address"  required><?php echo(!empty($users_data['address']))?$users_data['address']:"";?></textarea>
                                        <label class="form-label">Address</label>
                                    </div>
                                    <div class="help-info"></div>
                                </div>
                                  <div class="form-group form-float">
                            		<label class="form-label select-input-label">Approver</label>
                                    <div class="form-line">
                                        <select class="form-control"  name="Approver" id="Approver" required  data-live-search="true" >
                                        	<option value="">-- Please select --</option>
                                        	<option value="Un-approver">Un-approver</option>
                                        	<option value="Approver">Approver</option>
	                                    </select>
                                        
                                    </div>
                                    <div class="help-info"></div>
                                </div>
								<?php if(!empty($users_data)){?>
								<div class="form-group form-float">
									<input type="checkbox" id="change_credentials" data-toggle="collapse" data-target="#collapseExample" aria-expanded="true" aria-controls="collapseExample" class="filled-in">
									<label for="change_credentials">Change Credentials</label>
									<div class="help-info"></div>
								</div>
								<?php } ?>
								<div data-toggle="collapse" class="collapse <?php echo(empty($users_data))?"in":"";?>" id="collapseExample" aria-expanded="true">
									<div class="form-group form-float">
										<div class="form-line">
											<input type="text" class="form-control" name="username" id="username" value="<?php echo(!empty($users_data['username']))?$users_data['username']:"";?>" <?php if(!empty($users_data['username'])){?> disabled required  <?php }?>> 
											<label class="form-label">User Name</label>
										</div>
										<div class="help-info"></div>
									</div>
									<div class="form-group form-float">
										<div class="form-line">
											<input type="password" class="form-control" name="password" id="password" required>
											<label class="form-label">Password</label>
										</div>
										<div class="help-info"></div>
									</div>
									<div class="form-group form-float">
										<div class="form-line">
											<input type="password" class="form-control" name="confirm_password"  id="confirm_password"required>
											<label class="form-label">Confirm Password</label>
										</div>
										<div class="help-info"></div>
									</div>
								</div>
                                <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                                <a href="<?php echo base_url($this->router->fetch_module());?>" class="btn btn-primary waves-effect" type="button">Cancel</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Widgets -->
        </div>
    </section>
<script>
	var vRules = {
		"first_name":{required:true},
		"last_name":{required:true},
		"email_id":{required:true, email:true,remote: {
						/*url:"<?php echo base_url('users/dataEmailExist');?>" ,*/
						url:"<?php echo base_url($this->router->fetch_module().'/dataEmailExist');?>" ,
						type: "post",
						data: {email_id: function() {return $("#email_id").val();},user_id:function() {return $("#user_id").val();}}
					  }},
		"phone_number":{required:true,digits:true,minlength:10,maxlength:10},
		"address":{required:true},
		"username":{required:true,remote: {
						/*url:"<?php echo base_url('users/dataUsernameExist');?>" ,*/
						url:"<?php echo base_url($this->router->fetch_module().'/dataUsernameExist');?>" ,
						type: "post",
						data: {username: function() {return $( "#username" ).val();},user_id:function() {return $( "#user_id" ).val();}}
					  }},
		"password":{required:true},
		"confirm_password":{required:true,equalTo:"#password"},
	};
	var vMessages = {
		"first_name":{required:"Please Enter the First Name"},
		"last_name":{required:"Please Enter the Last Name"},
		"email_id":{required:"Please Enter the Email Id",email:"Please enter correct email id",remote:"Email ID already exist.."},
		"phone_number":{required:"Please Enter the Phone Number",digits:"Please enter valid phone no."},
		"address":{required:"Please Enter the Address"},
		"username":{required:"Please Enter the User Name",remote:"Username already exist.."},
		"password":{required:"Please Enter the Password"},
		"confirm_password":{required:"Please Enter the Confirm Password",equalTo:"Confirm Password is Mismatched"},
	};

	$("#form-validate").validate({
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form) 
		{
			var act = "<?php echo base_url($this->router->fetch_module()); ?>/submitForm";
			$("#form-validate").ajaxSubmit({
				url: act, 
				type: 'post',
				dataType: 'json',
				cache: false,
				clearForm: false, 
				beforeSubmit : function(arr, $form, options){
					$(".btn-primary").hide();
				},
				success: function (response) 
				{
					$(".btn-primary").show();
					if(response.success)
					{
					    swal({title: "Success!",text:response.msg,confirmButtonColor: "#a81f3b",imageUrl: "<?php echo base_url('assets/images/thumbs-up.png');?>"});
				        setTimeout(function(){
							window.location = "<?php echo base_url($this->router->fetch_module());?>";
						},2000);
					}else{	
						swal(response.msg);
						return false;
					}
				}
			});
		}
	});
</script>