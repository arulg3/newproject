<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Products</h2>
            </div>
            <!-- Widgets -->
            <!-- Widgets -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="body">
						<ol class="breadcrumb align-right">
							<li><a href="<?php echo base_url("home");?>"><i class="material-icons">home</i> Home</a></li>
							<li class="active"><i class="material-icons">person</i>Products</li>
						</ol>
					</div>
                    <div class="card">
						<div class="header text-right">
							<h2 class="pull-left">Products</h2>
							<button class="btn btn-primary" data-toggle="collapse" data-target="#serchfilter">Filter</button>
							<a href="<?php echo base_url($this->router->fetch_module());?>/addEdit" class="btn btn-primary waves-effect">Add Products</a>
						</div>
						<div class="panel panel-default search-panel" >
							<div class="panel-body collapse" id="serchfilter">
								<div class="col-md-3 col-sm-13 col-lg-3 col-xs-12 dataTables_filter">
									<label>Category</label>
									<!-- <input id="sSearch_0" name="sSearch_0" type="text" class="searchInput  form-control"> -->
									<select id="sSearch_0" name="sSearch_0"  class="searchInput  form-control" onchange="getProduct(this.value)">
										<option value="">All Category</option>
									    <?php 
									    if(!empty($categories) && count($categories) > 0){
									    	foreach($categories as $key => $val){
								    		?>
								    		 <option value="<?php echo $val['category_id']; ?>"><?php echo $val['category_name']; ?></option>
								    		<?php
									    	}
									    }
									    ?>
									</select>
								</div>
				
								<div class="col-md-3 col-sm-13 col-lg-3 col-xs-12 dataTables_filter">
									<label>Product Name</label>
		
									<select id="sSearch_1" name="sSearch_1" class="searchInput  form-control">
										<option value="">All Product</option>
								
									</select>
								</div>

								<div class="col-md-3 col-sm-13 col-lg-3 col-xs-12 dataTables_filter">
									<label>Product Size</label>
									<input id="sSearch_2" name="sSearch_2" type="text" class="searchInput  form-control">
								</div>
								<div class="col-md-3 col-sm-12 col-lg-3 col-xs-12 dataTables_filter">
									<label>Status</label>
									<select id="sSearch_4" name="sSearch_4" class="searchInput form-control show-tick" width="100%">
									    <option value="">All Status</option>
									    <option value="">All Status</option>
									    <option value="Active">Active</option>
									    <option value="In-active">In-active</option>
									</select>
								</div>
								<div class="col-md-3 col-sm-13 col-lg-3 col-xs-12">
									<button class="btn btn-primary mt25 waves-effect" onclick="clearSearchFilters();">Clear Search</button>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="body desktop_data">
							<div class="table-responsive">
								<div class="box-content">
									<div class="table-responsive">
										<table cellpadding="0" cellspacing="0" border="0" class=" dynamicTable table table-bordered" width="100%">
											<thead>
												<tr>
													<th>Category Name</th>
													<th>Product Name</th>
													<th>Product Size</th>
													<th>Product Quantity</th>
													<th>Product Image</th>
													<th>Status</th>
													<th data-bSortable = "false" >Change Status</th>
													<th>Actions</th>
												</tr>
											</thead>
											<tbody>
											</tbody>
											<tfoot>
											</tfoot>
										</table>
									</div>
								</div>
							</div>
						</div>
                    </div>
                </div>
				<hr>
			</div>	
            <!-- #END# Widgets -->
        </div>
    </section>
	<script>
	
	function changeStatus(user_id,elem) {
	if($(elem).is(":checked") == true){
		$(elem).prop("checked",false);
	}else{
		$(elem).prop("checked",true);
	}
	swal({
		title: "Are you sure?",
		text: "To Change the Status.",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#a81f3b",
		confirmButtonText: "Yes, Change it !",
		closeOnConfirm: false
	}, function () {
		$.ajax({
			url: "<?php echo base_url($this->router->fetch_module());?>/changeStatus",
			async: false,
			data : { id : user_id},
			type: "POST",
			dataType: "json",
			success: function (response){
				if(response.success){
					swal({title: "Success!",text: "Status Changed Successfully.",confirmButtonColor: "#a81f3b",imageUrl: "<?php echo base_url('assets/images/thumbs-up.png');?>"});
					if($(elem).is(":checked") == true){
						$(elem).prop("checked",false);
					}else{
						$(elem).prop("checked",true);
					}
					clearSearchFilters();
				}else{
					swal("Problem in Changing Status!");
				}
			}
		});
		
	});
}

function getProduct(category_id){
	// alert("inside function");
	$.ajax({
		url: "<?php echo base_url($this->router->fetch_module());?>/getProduct",
		async: false,
		data : { category_id : category_id},
		type: "POST",
		dataType: "json",
		success: function (response){
			// alert("got the response");
			// alert(response);
			console.log(response);
			if(response.success){

				// clearSearchFilters();
				// $('#sSearch_1').html('');
				$('select#sSearch_1').html(response.option);
				$('select#sSearch_1').selectpicker('refresh');
				// select();
			}else{
				clearSearchFilters();
			}
		}
	});
}

/* function showAjaxLoaderMessage() {
	setTimeout(function () {
		swal({title: "Success!",text: "Data Submited Successfully.",imageUrl: "<?php echo base_url('assets/images/thumbs-up.png');?>"});
	}, 5000);
} */
	</script>