<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Company extends CI_Controller 
{	
	function __construct(){
		parent::__construct();
		$this->load->model('companymodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
		checklogin();
	}
 
	function index(){
		$this->load->view('template/header.php');
		$this->load->view('company/index');
		$this->load->view('template/footer.php');
	}
	
	function addEdit(){
		$record_id = "";
		$edit_datas = array();
		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$record_id = $url_prams['id'];
			$result = $this->common->getData("tbl_company","*",array("company_id"=>$record_id));
			if(!empty($result)){
				$edit_datas['company_data'] = $result[0];
			}
		}
		$this->load->view('template/header.php');
		$this->load->view('company/addEdit',$edit_datas);
		$this->load->view('template/footer.php');
	}
	function fetch(){
		$get_result = $this->companymodel->getRecords($_GET);
		$result = array();
		$result["sEcho"]= $_GET['sEcho'];
		$result["iTotalRecords"] = $get_result['totalRecords'];	//iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"]= $get_result['totalRecords']; //iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result']) && count($get_result['query_result']) > 0){
			for($i=0;$i<sizeof($get_result['query_result']);$i++){
				$temp = array();
				array_push($temp, ucfirst($get_result['query_result'][$i]->company_name));
				array_push($temp, ucfirst($get_result['query_result'][$i]->company_address));
				array_push($temp, $get_result['query_result'][$i]->company_spoc_name);
				array_push($temp, $get_result['query_result'][$i]->company_spoc_phone_number);
				array_push($temp, $get_result['query_result'][$i]->status);
				
				$status_change = "";
				$actionCol = "";
				$status_type = '';
				if($get_result['query_result'][$i]->status == 'Active'){
					$status_type = "checked = 'checked'";
				}
				
				$status_change = '<div style="width:75px;float:right"><div class="switch">
												<label>
													<input type="checkbox" class="status_change" onchange="changeStatus('.$get_result['query_result'][$i]->company_id.',this)"  '.$status_type.'">
													<span class="lever"></span>
												</label>
											</div></div>';
				array_push($temp, $status_change);
				
				$actionCol .='<div style="width:75px;float:right;text-align: center;"><a href="company/addEdit?text='.rtrim(strtr(base64_encode("id=".$get_result['query_result'][$i]->company_id), '+/', '-_'), '=').'" title="Edit" class="text-center"><i class="material-icons ">create</i></a></div>';
				array_push($temp, $actionCol);
				
				array_push($items, $temp);
			}
		}
		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}
	
	function submitForm(){
		// echo "<pre>";
		// print_r($_POST);
		// exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

			/*check duplicate entry*/	
			$condition = "company_name = '".$this->input->post('company_name')."' ";
			if(!empty($this->input->post("company_id"))){
				$condition .= " AND company_id <> ".$this->input->post("company_id");
			}	

			$chk_company_sql = $this->common->Fetch("tbl_company","company_id",$condition);
			$rs_company = $this->common->MySqlFetchRow($chk_company_sql, "array");

			if(!empty($rs_company[0]['company_id'])){
				echo json_encode(array('success'=>false, 'msg'=>'Company name already exist...'));
				exit;
			}

			$data = array();
			$data['company_name'] = $this->input->post('company_name');
			$data['company_address'] = trim($this->input->post('company_address'));
			$data['company_spoc_name'] = $this->input->post('company_spoc_name');
			$data['company_spoc_phone_number'] = $this->input->post('company_spoc_phone_number');
			if(!empty($this->input->post("company_id"))){
			    $data['updated_by'] = $_SESSION['jain_decorator_admin'][0]['user_id'];
				$data['updated_on'] = date("Y-m-d H:i:s");
				$result = $this->common->updateData("tbl_company",$data,array("company_id"=>$this->input->post("company_id")));
				if($result){
					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while Updating data.'));
					exit;
				}
			}else{
				$data['created_by'] = $_SESSION['jain_decorator_admin'][0]['user_id'];
				$data['created_on'] = date("Y-m-d H:i:s");
				$result = $this->common->insertData("tbl_company",$data,"1");
				if($result){
					echo json_encode(array('success'=>true, 'msg'=>'Record Inserted Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while Inserting data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem while add/edit data.'));
			exit;
		}
	}
	
	function changeStatus(){
		if(!empty($this->input->post("id"))){
			$existing_data = $this->common->getData("tbl_company","status",array('company_id'=>$this->input->post("id")));
			$data = array();
			if(!empty($existing_data) && $existing_data[0]['status'] == "Active"){
				$data['status'] = "In-active";
			}else{
				$data['status'] = "Active";
			}
			$result = $this->common->updateData("tbl_company",$data,array("company_id"=>$this->input->post("id")));
			if($result){
				echo json_encode(array('success'=>true));
				exit;
			}else{
				echo json_encode(array('success'=>false));
			}
		}
	}

	function dataCompanynameExist(){
		
		$condition = array("company_name"=>$_POST['company_name']);

		if(!empty($_POST['company_id']) && $_POST['company_id'] !=""){
			$condition['company_id <>'] = $_POST['company_id'];
		}

		$result=$this->common->getData("tbl_company","*",$condition);
				
		// echo $this->db->last_query();
		// echo "<pre>";
		// print_r($result);
		// exit;
				
		if($result > 0){
			echo  json_encode(FALSE);
		}else{
			echo  json_encode(TRUE);
		} 
	} 

}

?>
