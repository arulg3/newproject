<?php
if (!defined('BASEPATH'))
exit('No direct script access allowed');
class Aboutus extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('aboutusmodel', '', TRUE);
    }
    function index()
    {
        $this->load->view('templates/header.php');
        $this->load->view('Aboutus/index');
        $this->load->view('templates/footer.php');
    }
}
?>