<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<title>NM</title>
		<!-- Favicon-->
		<!-- Google Fonts -->
	<!--------------------------------CSS START----------------------------------->
		<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
		<!-- Bootstrap Core Css -->
        <!-- <link href="<?php echo base_url();?>assets/js/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet"> -->
		<link href="<?php echo base_url();?>assets/js/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
		 <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
		<!-- Waves Effect Css -->
		<link href="<?php echo base_url();?>assets/js/plugins/node-waves/waves.css" rel="stylesheet" />
		<!-- Animation Css -->
            <link href="<?php echo base_url();?>assets/css/lightbox.min.css" rel="stylesheet" />
        <!--  for light box-->
		<link href="<?php echo base_url();?>assets/js/plugins/animate-css/animate.css" rel="stylesheet" />
		<!-- Morris Chart Css-->
		<link href="<?php echo base_url();?>assets/js/plugins/morrisjs/morris.css" rel="stylesheet" />
		
		<!-- sweet alert Css-->
		<link href="<?php echo base_url();?>assets/js/plugins/sweetalert/sweetalert.css" rel="stylesheet" />
		
		<!-- Custom Css -->
		<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
		<!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
		<link href="<?php echo base_url();?>assets/css/themes/all-themes.css" rel="stylesheet" />
		
		<!---datatable-->
		<link href="<?PHP echo base_url();?>assets/js/plugins/data_table/css/jquery.dataTables.css" rel="stylesheet">
		<link href="<?PHP echo base_url();?>assets/js/plugins/data_table/css/rowReorder.dataTables.min.css" rel="stylesheet">
		<link href="<?PHP echo base_url();?>assets/js/plugins/data_table/css/responsive.dataTables.min.css" rel="stylesheet">
		
		<!---selectbox-->
		<link href="<?PHP echo base_url();?>assets/js/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet">

        <!--Datepicker CSS--->
        <link href='<?PHP echo base_url();?>assets/js/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css'>

        <!--Daterangepicker CSS--->
        <link href='<?PHP echo base_url();?>assets/js/plugins/bootstrap-daterangepicker/css/daterangepicker.css'>

<!--------------------------------CSS END----------------------------------->

<!--------------------------------script start----------------------------------->
		  <!-- Jquery Core Js -->
		<script src="<?php echo base_url();?>assets/js/plugins/jquery/jquery.min.js"></script>
		
         <!-- <script src="../dist/js/lightbox-plus-jquery.min.js"></script> -->
        <script src="<?php echo base_url();?>assets/js/lightbox.js"></script>

		<!-- Bootstrap Core Js -->
		<script src="<?php echo base_url();?>assets/js/plugins/bootstrap/js/bootstrap.js"></script>
		
		<!-- Select Plugin Js -->
		<script src="<?php echo base_url();?>assets/js/plugins/bootstrap-select/js/bootstrap-select.js"></script>
		
		<!-- Slimscroll Plugin Js -->
		<script src="<?php echo base_url();?>assets/js/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
		
		<!-- Waves Effect Plugin Js -->
		<script src="<?php echo base_url();?>assets/js/plugins/node-waves/waves.js"></script>
		
		<!-- SWEET ALERT Plugin Js -->
		<script src="<?php echo base_url();?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/plugins/sweetalert/sweetalert-dev.js"></script>
        		
		<!-- Custom Js -->
		<script src="<?php echo base_url();?>assets/js/admin.js"></script>
		
		<!-- Demo Js -->
		<script src="<?php echo base_url();?>assets/js/demo.js"></script>
		
		<!-- Select Js 
		<script src="<?php echo base_url();?>assets/js/plugins/bootstrap-select/js/bootstrap-select.js"></script>
		-->
		<!--Data table Js-->
		<script src='<?PHP echo base_url();?>assets/js/plugins/data_table/js/jquery.dataTables.min.js'></script>
		<script src='<?PHP echo base_url();?>assets/js/plugins/data_table/js/datatable.js'></script>
		<script src='<?PHP echo base_url();?>assets/js/plugins/data_table/js/dataTables.rowReorder.min.js'></script>
		<script src='<?PHP echo base_url();?>assets/js/plugins/data_table/js/dataTables.responsive.min.js'></script>
		
		<!--Form Validate--->
		<script src='<?PHP echo base_url();?>assets/js/plugins/jquery-validation/jquery.validate.js'></script>
		<script src='<?PHP echo base_url();?>assets/js/plugins/jquery-validation/additional-methods.js'></script>
        <script src='<?PHP echo base_url();?>assets/js/plugins/jquery-validation/form-validation.js'></script>
        <script src='<?PHP echo base_url();?>assets/js/plugins/jquery-validation/jquery.form.js'></script>

        <!--Moment JS--->
        <script src='<?PHP echo base_url();?>assets/js/plugins/momentjs/moment.js'></script>

        <!--Datepicker JS--->
        <script src='<?PHP echo base_url();?>assets/js/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js'></script>

        <!--Daterangepicker JS--->
        <script src='<?PHP echo base_url();?>assets/js/plugins/bootstrap-daterangepicker/js/daterangepicker.js'></script>

       
        
        <!-- Tooltip -->
        <script>
        $(function () {
            //Tooltip
            $('[data-toggle="tooltip"]').tooltip({
                container: 'body'
            });

            //Popover
            $('[data-toggle="popover"]').popover();
        });
        </script>
       

       
<!--------------------------------script End----------------------------------->
	</head>
    <?php 
        $login_class = "";
        $theme_class ="theme-white";
        if(($this->uri->segment(1)=="login" || $this->uri->segment(1)=="") && empty($_SESSION['jain_decorators_admin'])){
            $login_class = "display_none";
            $theme_class ="login-page";
        }
    ?>
    <div class ="<?php echo $login_class;?>">
    	<nav class="navbar ">
            <div class="container-fluid">
                <div class="navbar-header">
                   <!-- <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>-->
                    <a href="javascript:void(0);" class="bars"></a>
                    <a class="navbar-brand" href="<?php echo base_url("home");?>"><img src="<?php echo base_url("assets/images/logo.png");?>" class="img-responsive"></a>
                </div>
            </div>
        </nav>
    	<section class="<?php echo $login_class;?>">
            <!-- Left Sidebar -->
            <aside id="leftsidebar" class="sidebar">
                <!-- User Info -->
                <div class="user-info">
                    <div class="image">
                        <img src="<?php echo base_url("assets/images/user.png")?>" width="48" height="48" alt="User" />
                    </div>
                    <div class="info-container">
                        <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo(!empty($_SESSION['jain_decorator_admin']))?ucfirst($_SESSION['jain_decorator_admin'][0]['first_name']):"User";?></div>
                        <div class="email"><?php echo(!empty($_SESSION['jain_decorator_admin']))?ucfirst($_SESSION['jain_decorator_admin'][0]['email_id']):"";?></div>
                        <div class="btn-group user-helper-dropdown">
                            <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="<?php echo base_url("profile")?>"><i class="material-icons">person</i>Profile</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="<?php echo base_url("home/logout")?>"><i class="material-icons">input</i>Sign Out</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- #User Info -->
                <!-- Menu -->
                <div class="menu">
                    <ul class="list">
                        <li class="<?php echo($this->uri->segment(1) == "home" || $this->uri->segment(1) =="")?"active":"";?>">
                            <a href="<?php echo base_url("home")?>">
                                <i class="material-icons">home</i>
                                <span>Home</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </aside>
            <!-- #END# Left Sidebar -->
        </section>
    </div>
	<body class="<?php echo $theme_class;?>">