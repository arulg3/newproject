<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Users extends CI_Controller 
{	
	function __construct(){
		parent::__construct();
		$this->load->model('usersmodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
		if(!in_array($_SESSION['jain_decorator_admin'][0]['user_type'],array("1"))){
			redirect("home");
		}
		checklogin();
	}
 
	function index(){
		$this->load->view('template/header.php');
		$this->load->view('users/index');
		$this->load->view('template/footer.php');
	}

	function addEdit(){
		$record_id = "";
		$edit_datas = array();
		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$record_id = $url_prams['id'];
			$result = $this->common->getData("tbl_users","*",array("user_id"=>$record_id));
			if(!empty($result)){
				$edit_datas['users_data'] = $result[0];
			}
		}
		$this->load->view('template/header.php');
		$this->load->view('users/addEdit',$edit_datas);
		$this->load->view('template/footer.php');
	}

	function fetch(){
		$get_result = $this->usersmodel->getRecords($_GET);
		$result = array();
		$result["sEcho"]= $_GET['sEcho'];
		$result["iTotalRecords"] = $get_result['totalRecords'];	//iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"]= $get_result['totalRecords']; //iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result']) && count($get_result['query_result']) > 0){
			for($i=0;$i<sizeof($get_result['query_result']);$i++){
				$temp = array();
				array_push($temp, ucfirst($get_result['query_result'][$i]->first_name.' '.ucfirst($get_result['query_result'][$i]->last_name)));
				// array_push($temp, ucfirst($get_result['query_result'][$i]->last_name));
				array_push($temp, $get_result['query_result'][$i]->email_id);
				array_push($temp, $get_result['query_result'][$i]->phone_number);
				array_push($temp, $get_result['query_result'][$i]->address);
				array_push($temp, $get_result['query_result'][$i]->status);
				$status_change = "";
				$actionCol = "";
				$status_type = '';
				$approver_type=' ';
				if($get_result['query_result'][$i]->status == 'Active'){
					$status_type = "checked = 'checked'";
				}
				if($get_result['query_result'][$i]->approver == 'Approver'){
					$approver_type = "checked = 'checked'";
				}

				$approver_change = '<div style="width:75px;float:right"><div class="switch">
												<label>
													<input type="checkbox" class="approver_change" onchange="changeapprover('.$get_result['query_result'][$i]->user_id.',this)"  '.$approver_type.'">
													<span class="lever"></span>
												</label>
												<div class="clearfix"></div>
											</div></div>';
				array_push($temp, $approver_change);

				$status_change = '<div style="width:75px;float:right"><div class="switch">
												<label>
													<input type="checkbox" class="status_change" onchange="changeStatus('.$get_result['query_result'][$i]->user_id.',this)"  '.$status_type.'">
													<span class="lever"></span>
												</label>
												<div class="clearfix"></div>
											</div></div>';
				array_push($temp, $status_change);
				
				$actionCol .='<div style="width:75px;float:right;text-align: center;"><a href="users/addEdit?text='.rtrim(strtr(base64_encode("id=".$get_result['query_result'][$i]->user_id), '+/', '-_'), '=').'" title="Edit" class="text-center"><i class="material-icons ">create</i></a></div>';
				array_push($temp, $actionCol);
				array_push($items, $temp);
			}
		}
		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}
	
	function submitForm(){
		// echo "<pre>";
		// print_r($_POST);
		// exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{

			/*check duplicate entry*/	
			$condition = "email_id = '".$this->input->post('email_id')."' ";
			if(!empty($this->input->post("user_id"))){
				$condition .= " AND user_id <> '".$this->input->post("user_id")."' ";
			}	
			$chk_username_sql = $this->common->Fetch("tbl_users","user_id",$condition);
			// print_r($chk_username_sql);
			$rs_username = $this->common->MySqlFetchRow($chk_username_sql, "array");
			// print_r($rs_username);
			// exit();
			if(!empty($rs_username[0]['user_id'])){
				echo json_encode(array('success'=>false, 'msg'=>'Email id already exist...'));
				exit;
			}

			$data = array();
			$data['first_name'] = $this->input->post('first_name');
			$data['last_name'] = $this->input->post('last_name');
			$data['email_id'] = $this->input->post('email_id');
			$data['phone_number'] = $this->input->post('phone_number');
			$data['address'] = trim($this->input->post('address'));
			$data['user_type'] = "2";
			if(!empty($this->input->post("user_id"))){
			    if(!empty($this->input->post('change_credentials')) && $this->input->post('change_credentials') == "on"){

			    	/*check duplicate entry*/	
					$condition = "username = '".$this->input->post('username')."' ";
					if(!empty($this->input->post("user_id"))){
						$condition .= " AND user_id <> '".$this->input->post("user_id")."' ";
					}	
					$chk_email_sql = $this->common->Fetch("tbl_users","user_id",$condition);
					$rs_email = $this->common->MySqlFetchRow($chk_email_sql, "array");
					if(!empty($rs_email[0]['user_id'])){
						echo json_encode(array('success'=>false, 'msg'=>'Username already exist...'));
						exit;
					}

			        $data['username'] = $this->input->post('username');
			    	$data['password'] = md5($this->input->post('password'));
			    }
				$result = $this->common->updateData("tbl_users",$data,array("user_id"=>$this->input->post("user_id")));
				if($result){
					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while Updating data.'));
					exit;
				}
			}else{
				$data['username'] = $this->input->post('username');
				$data['password'] = md5($this->input->post('password'));
				$data['approver']=$this->input->post('Approver');
				$result = $this->common->insertData("tbl_users",$data,"1");
				if($result){
					// /* Sent mail to Employee email id */
					// $result_content = $this->employee_model->getEmailContent($this->input->post("user_id"));
											
					// $regi_link = FRONT_URL."/employee/registration?text=".rtrim(strtr(base64_encode("id=".$employee_id), '+/', '-_'), '=');
									
					// $logo_image = '<a href="'.FRONT_URL.'"><img height="50px" src="'.base_url().'images/Final_Logo_Contetra.jpg" width="150px" /></a>';

					// $message = str_replace(array('{logo_image}','{link}'), array($logo_image,$regi_link), $result_content['content']);
					$this->email->from(FROM_EMAIL); 	// change it to yours
					$this->email->to('guptashivshankar.573@gmail.com');	// change it to yours
					$this->email->subject("user creatation for jain Decorator");
					$this->email->message("You Accoutn Has been Successfully created");		
					$this->email->send();
					// 						/* End */
					echo json_encode(array('success'=>true, 'msg'=>'Record Inserted Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while Adding data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem while add/edit data.'));
			exit;
		}
	}
	
	function changeStatus(){
		if(!empty($this->input->post("id"))){
			$existing_data = $this->common->getData("tbl_users","status",array('user_id'=>$this->input->post("id")));
			$data = array();
			if(!empty($existing_data) && $existing_data[0]['status'] == "Active"){
				$data['status'] = "In-active";
			}else{
				$data['status'] = "Active";
			}
			$result = $this->common->updateData("tbl_users",$data,array("user_id"=>$this->input->post("id")));
			if($result){
				echo json_encode(array('success'=>true));
				exit;
			}else{
				echo json_encode(array('success'=>false));
			}
		}
	}

		function changeapprover(){
		if(!empty($this->input->post("id"))){
			$existing_data = $this->common->getData("tbl_users","approver",array('user_id'=>$this->input->post("id")));
			$data = array();
			if(!empty($existing_data) && $existing_data[0]['approver'] == "Approver"){
				$data['approver'] = "Un-approver";
			}else{
				$data['approver'] = "Approver";
			}
			$result = $this->common->updateData("tbl_users",$data,array("user_id"=>$this->input->post("id")));
			if($result){
				echo json_encode(array('success'=>true));
				exit;
			}else{
				echo json_encode(array('success'=>false));
			}
		}
	}

	function dataEmailExist(){
		
		$condition = array("email_id"=>$_POST['email_id']);

		if(!empty($_POST['user_id']) && $_POST['user_id'] !=""){
			$condition['user_id <>'] = $_POST['user_id'];
		}
		
		$result=$this->common->getData("tbl_users","*",$condition);
		
		// echo "<pre>";
		// print_r($result);
		// exit;

		if($result > 0){
			echo  json_encode(FALSE);
		}else{
			echo  json_encode(TRUE);
		}
	}
	 
	function dataUsernameExist(){
		
		$condition = array("username"=>$_POST['username']);
		// print_r($condition);
		// print_r($_POST['user_id']);
		// exit();

		if(!empty($_POST['user_id']) && $_POST['user_id'] !=""){
			$condition['user_id <>'] = $_POST['user_id'];
		}
		
		$result=$this->common->getData("tbl_users","*",$condition);
		
		// echo "<pre>";
		// print_r($result);
		// exit;

		if($result == 0){
			echo  json_encode(true);
		}else{
			echo  json_encode(false);
		} 
	} 

}

?>
