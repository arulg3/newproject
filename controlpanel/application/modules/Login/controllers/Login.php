<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller 
{	
	function __construct(){
		parent::__construct();
		$this->load->model('loginmodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
		if(!empty($_SESSION["sevasadan_control_admin"])){
			redirect("home");
		}
	}
	
	function index(){
		$this->load->view('template/header.php');
		$this->load->view('login/index');
		$this->load->view('template/footer.php');
	}

	function loginValidate(){
		$result = $this->common->getData("tbl_users","*",array("username"=>$_POST['username'],"password"=>md5($_POST['password']),"status"=>"Active"));
		if($result){
			$_SESSION["sevasadan_control_admin"] = $result;
			echo json_encode(array("success"=>true, "msg"=>'You are successfully logged in.'));
			exit;		
		}else{
			echo json_encode(array("success"=>false, "msg"=>'Username or Password incorrect.'));
			exit;
		}
	}
	
	function forgotpassword()
	{

		
		if(!empty($_POST['email_id'])){
			$result = $this->loginmodel->forgotPassword($_POST['email_id']);	
				// echo "<pre>"; 
				// print_r($result);	exit;					 
			if(!empty($result)){
				$result_content = $this->loginmodel->getEmailContent();
				$logo_image = '<a href="'.base_url().'"><img height="50px" src="'.base_url().'images/JD-logo.png" width="150px" /></a>';
				$url = base_url()."forgetchangepassword?text=".rtrim(strtr(base64_encode("eid=".$_POST['email_id']), '+/', '-_'), '=')."/".rtrim(strtr(base64_encode("dt=".date("Y-m-d")), '+/', '-_'), '=');						
				$message = str_replace(array('{logo_image}','{username}','{url}'), array($logo_image,$result[0]['first_name']." ".$result[0]['last_name'],$url), $result_content['content']);								
				$this->email->from(FROM_EMAIL); 	// change it to yours
				$this->email->to("info@attoinfotech.com");	// change it to yours
				$this->email->subject(str_replace("User","Employee User",$result_content['subject']));
				$this->email->message($message);		
				$checkemail = $this->email->send();
				if($checkemail){
					echo json_encode(array("success"=>true, "msg"=>'Mail sent successfully, Please check your mail.'));
					exit;
				}else	{
					echo json_encode(array("success"=>false, "msg"=>'Problem while sending mail..'));
					exit;
				}			
			}else{		
				echo json_encode(array("success"=>false, "msg"=>'Invalid Email ID.'));
				exit;
			}
		}else{		
			echo json_encode(array("success"=>false, "msg"=>'Please provide Email ID.'));
			exit;
		}	
	}
	
}

?>
