<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Website Pages</h2>
            </div>
            <!-- Widgets -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					
					<div class="body">
						<ol class="breadcrumb align-right">
							<li><a href="<?php echo base_url("home");?>"><i class="material-icons">home</i> Home</a></li>
							<li><a href="<?php echo base_url($this->router->fetch_module());?>"><i class="material-icons">list</i> Website Pages</a></li>
							<li class="active"><i class="material-icons">mode_edit</i>Add Edit</li>
						</ol>
					</div>
                    <div class="card">
                        <div class="header">
                            <h2>Add / Edit Website Pages</h2>
                        </div>
                        <div class="body">
                            <form id="form-validate" class="form-validate" name="form-validate" method="POST" enctype="multipart/form-data">
								<input type="hidden" name="website_page_id" id="website_page_id" value="<?php echo(!empty($content_data['website_page_id']))?$content_data['website_page_id']:"";?>">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="website_page_name" id="website_page_name" value="<?php echo(!empty($content_data['website_page_name']))?$content_data['website_page_name']:"";?>" required>
                                        <label class="form-label">Webpage Name</label>
                                    </div>
								</div>
								<div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="website_page_heading" id="website_page_heading" value="<?php echo(!empty($content_data['website_page_heading']))?$content_data['website_page_heading']:"";?>" >
                                        <label class="form-label">Webpage Heading</label>
                                    </div>
                                </div>
                                <div class="form-group form-float">
									<label class="form-label">Webpage Banner</label>
                                    <div class="form-line">
                                        <input type="file" class="form-control" name="website_page_banner" id="website_page_banner">
                                    </div>
									<?php if(!empty($content_data['website_page_banner'])){?>
										<div class="imagewrap">
											<button type="button" class="btn btn-primary image_remove_icon waves-effect" onclick = "deleteFile('<?php echo $content_data['website_page_id']?>','<?php echo $content_data['website_page_banner'];?>')"><i class="material-icons" >delete_forever</i></button>
											<img src="<?php echo FRONT_URL."/uploads/webpage_images/".$content_data['website_page_banner'];?>" class="preview_uploaded_image">
										</div>
										<input type="hidden" name="existing_website_page_banner" id="existing_website_page_banner" value="<?php echo(!empty($content_data['website_page_banner']))?$content_data['website_page_banner']:"";?>">
									<?php }?>
                                    <div class="help-info"></div>
								</div>
                                <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                                <a href="<?php echo base_url($this->router->fetch_module());?>" class="btn btn-primary waves-effect" type="button">Cancel</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Widgets -->
        </div>
    </section>
<script>
	var vRules = {
		"website_page_name":{required:true,remote: {
							url:"<?php echo base_url($this->router->fetch_module());?>/dataExist" ,
							type: "post",
							data: {website_page_name: function() {return $( "#website_page_name" ).val();},website_page_id:function() {return $( "#website_page_id" ).val();}}
						}},
	};
	var vMessages = {
		"website_page_name":{required:"Please Enter the Webpages Name",remote:"Webpages name already exist."},
	};

	$("#form-validate").validate({
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form) 
		{
			var act = "<?php echo base_url($this->router->fetch_module());?>/submitForm";
			$("#form-validate").ajaxSubmit({
				url: act, 
				type: 'post',
				dataType: 'json',
				cache: false,
				clearForm: false, 
				beforeSubmit : function(arr, $form, options){
					$(".btn-primary").hide();
				},
				success: function (response) 
				{
					$(".btn-primary").show();
					if(response.success)
					{
					    swal({title: "Success!",text:response.msg,confirmButtonColor: "#e5922e",imageUrl: "<?php echo base_url('assets/images/thumbs-up.png');?>"});
						setTimeout(function(){
							window.location = "<?php echo base_url($this->router->fetch_module());?>";
						},2000);
					}else{	
						swal(response.msg);
						return false;
					}
				}
			});
		}
	});

	function deleteFile(id,name) {
		swal({
			title: "Are you sure?",
			text: "To Delete this File.",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#e5922e",
			confirmButtonText: "Yes, Delete it !",
			closeOnConfirm: false
		}, function () {
			$.ajax({
				url: "<?php echo base_url($this->uri->segment(1))?>/deleteFile",
				async: false,
				data : { id : id,name: name},
				type: "POST",
				dataType: "json",
				success: function (response){
					if(response.success){
						swal({title: "Success!",text: "File Deleted Successfully.",confirmButtonColor: "#e5922e",imageUrl: "<?php echo base_url('assets/images/thumbs-up.png');?>"});
						setTimeout(function(){
							window.location = "<?php echo base_url($this->router->fetch_module());?>";
						},2000);
					}else{
						swal("Problem in Deleting File!");
						return false;
					}
				}
			});
			
		});
	}
</script>