<div class="container">
	<section id="formHolder">
		<div class="row">
         <!-- Brand Box -->
			<div class="col-sm-6 brand">
				<div class="heading">
					<a href="<?php echo base_url();?>" class="logo"><img src="<?php echo base_url("images/logo.png");?>" class="img-responsive"> </a>
				</div>
				<div class="success-msg">
					<p>Great! You are one of our members now</p>
					<a href="#" class="profile">Your Profile</a>
				</div>
			</div>
			<!-- Form Box -->
			<div class="col-sm-6 form">
				<!-- Login Form -->
				<div class="login form-peice ">
				   <form class="login-form" action="#" method="post">
				   		<div class="form-group">
							<label for="Username">Username</label>
							<input type="text" name="username" id="username" class="username">
							<span class="error"></span>
						</div>
						<div class="form-group">
							<label for="Password">Password</label>
							<input type="password" name="password" id="password" class="password"  autocomplete="nope">
							<span class="error"></span>
						</div>
					  <div class="CTA">
						 <input type="submit" value="Login">
						 <a href="#" class="switch">Forgot Password ?</a>
					  </div>
				   </form>
				</div><!-- End Login Form -->
				
				<!-- Forgot Password Form -->
				<div class="forgot-password form-peice switched">
				   <form class="forgot-password-form" action="#" id="forgot-password-form" name="forgot-password-form" method="post">
						<div class="form-group">
							<label for="Username">Username</label>
							<input type="text" name="username" id="username" class="username">
							<span class="error"></span>
						</div>
						<div class="form-group">
							<label for="email">Email Adderss</label>
							<input type="email" name="email_id" id="email_id" class="email_id">
							<span class="error"></span>
						</div>
						<div class="CTA">
							<input type="submit" value="Submit" id="submit">
							<a href="#" class="switch">Back to Login</a>
						</div>
				   </form>
				</div><!-- End Forgot Password Form -->
			</div>
		</div>
	</section>
</div>
</body>

</html>