<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Product</h2>
            </div>
            <!-- Widgets -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					
					<div class="body">
						<ol class="breadcrumb align-right">
							<li><a href="<?php echo base_url("home");?>"><i class="material-icons">home</i> Home</a></li>
							<li><a href="<?php echo base_url($this->router->fetch_module());?>"><i class="material-icons">person</i>Products</a></li>
							<li class="active"><i class="material-icons">mode_edit</i> Add Edit</li>
						</ol>
					</div>
                    <div class="card">
                        <div class="header">
                            <h2>Add / Edit Products</h2>
                        </div>
                        <div class="body">
                            <form id="form-validate" class="form-validate" name="form-validate" method="POST">
								<input type="hidden" name="product_id" id="product_id" value="<?php echo(!empty($category_data['product_id']))?$category_data['product_id']:"";?>">
                                <div class="form-group form-float">
                            		<label class="form-label select-input-label">Category</label>
                                    <div class="form-line">
                                        <select class="form-control show-tick"  name="category_id" id="category_id" required  data-live-search="true" >
	                                        <option value="">-- Please select --</option>
	                                        <?php 

                                            // print_r($categories);
                                            // exit();
                                            if(!empty($categories) && count($categories) > 0) { 
                                        	foreach ($categories as $key => $value) {

                                        		$sel = (!empty($category_data['category_id']) && $category_data['category_id'] == $value['category_id']) ? "selected = 'selected' " : "";

                                    		?>
                                        	<option value="<?php echo $value['category_id']; ?>" <?php echo $sel; ?> ><?php echo $value['category_name']; ?></option>
                                    		<?php 
                                        	}
	                                        ?>
	                                        <?php } ?>
	                                    </select>
                                        
                                    </div>
                                    <div class="help-info"></div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="product_name" id="product_name" value='<?php echo(!empty($category_data['product_name']))?$category_data['product_name']:"" ;?>' required>
                                        <label class="form-label">Product Name</label>
                                    </div>
                                    <div class="help-info"></div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="product_size" id="product_size" value="<?php echo(!empty($category_data['product_size']))?$category_data['product_size']:"";?>" >
                                        <label class="form-label">Product Size</label>
                                    </div>
                                    <div class="help-info"></div>
                                </div>
                                <div class="form-group form-float">
                                    <label class="form-label">Product Image</label>
                                    <div class="form-line">
                                        <input type="file" class="form-control" name="product_image" id="product_image">
                                    </div>
                                    <?php if(!empty($category_data['product_image'])){?>
                                        <div class="imagewrap">
                                            <button type="button" class="btn btn-primary image_remove_icon waves-effect" onclick = "deleteFile('<?php echo $category_data['product_id']?>','<?php echo $category_data['product_image'];?>')"><i class="material-icons" >delete_forever</i></button>
                                            <img src="<?php echo FRONT_URL."/uploads/product_images/".$category_data['product_image'];?>" class="preview_uploaded_image">
                                        </div>
                                        <input type="hidden" name="existing_product_image" id="existing_product_image" value="<?php echo(!empty($category_data['product_image']))?$category_data['product_image']:"";?>">
                                    <?php }?>
                                    <div class="help-info"></div>
                                </div>
								<div class="form-group form-float">
                                    <div class="panel panel-default">
										<div class="panel-heading text-right">
											<button class="btn btn-sm btn-success add_more" type="button">
												<i class="fa fa-plus" style="font-size:15px;"></i> Add More
											</button>
										</div> 
										<div class="panel-body"> 
                                           <div class="table-responsive">
    	                                        <table class="table table-bordered table-striped" id="admore_table">
    	                                        	<thead>
    	                                        		<tr>
    		                                        		<th>Quantity</th>
    		                                        		<th>Location</th>
    		                                        		<th>Status</th>
    		                                        		<th>#</th>
    	                                        		</tr>
    	                                        	</thead>
    	                                        	<tbody class="tbody_container">
    	                                        	<?php 
    	                                        	if(!empty($product_locations) && count($product_locations) > 0){
                                            		foreach ($product_locations as $prod_loc_key => $prod_loc_val) {

                                                     $sql_product_locations = $this->common->Fetch("tbl_product_current_qty","*","product_id='".$prod_loc_val['product_id']."'");
                                                     $data=$this->common->MySqlFetchRow($sql_product_locations,"array");
                                                     // echo "<pre>";
                                                     // print_r($data[$prod_loc_key]['product_qty']);  
                                        			?>
                                        				<tr tr_count="<?php echo $prod_loc_key; ?>" class="active">
    	                                        			<td>
                                                                <input type="text" class="form-control required" name="product_qty[<?php echo $prod_loc_key; ?>]" id="product_qty<?php echo $prod_loc_key; ?>"  title="Please Enter the Quantity" value="<?php echo $data[$prod_loc_key]['product_qty']; ?>">
                                                                <input type="hidden" name="product_qty_location_id[<?php echo $prod_loc_key; ?>]" id="product_qty_location_id<?php echo $prod_loc_key; ?>" value="<?php echo $prod_loc_val['product_qty_location_id']; ?>">
                                                            </td>
                                                            <td>
                                                                <select class="selectpicker form-control show-tick product_locations required" name="product_location_selected[<?php echo $prod_loc_key; ?>]"   id="product_location_selected<?php echo $prod_loc_key; ?>" disabled>
                                                                    <option value="">-- Please select --</option>
                                                                    <?php if(!empty($locations) && count($locations) > 0) { 
                                                                    foreach ($locations as $key => $value) {

                                                                    	$sel_loc = ($value['location_id'] == $prod_loc_val['location_id']) ? "selected = 'selected' " : "";

                                                                    ?>
                                                                    <option value="<?php echo $value['location_id']; ?>"  <?php echo $sel_loc; ?> ><?php echo $value['location_name']." [".$value['location_code']."]"; ?></option>
                                                                    <?php 
                                                                    }
                                                                    ?>
                                                                    <?php } ?>
                                                                </select> 
                                                                <input type="hidden" name="product_location[<?php echo $prod_loc_key; ?>]" id="product_location<?php echo $prod_loc_key; ?>" value="<?php echo $prod_loc_val['location_id']; ?>">
                                                            </td>
                                                            <td>
                                                                <select class="form-control show-tick required" name="status[<?php echo $prod_loc_key; ?>]" id="status<?php echo $prod_loc_key; ?>">
                                                                    <option value="Active" <?php echo ($prod_loc_val['status'] == "Active") ? "selected = 'selected' " : ""; ?> >Active</option>
                                                                    <option value="Active" <?php echo ($prod_loc_val['status'] == "Active") ? "selected = 'selected' " : ""; ?> >Active</option>
                                                                    <option value="In-active" <?php echo ($prod_loc_val['status'] == "In-active") ? "selected = 'selected' " : ""; ?> >In-active</option>
                                                                </select>
                                                            </td>
    	                                        			<td>
    	                                        				<!--<button title="Remove" class="btn-danger btn btn-sm remove">
    																<i class="material-icons">remove</i>
    															</button>-->
    	                                        			</td>
    	                                        		</tr>
                                        			<?php
                                            		}}
                                            		else{
    	                                        	?>
    	                                        		<tr tr_count="0" class="active">
    	                                        			<td>
                                                                <input type="text" class="form-control required" name="product_qty[0]" id="product_qty0"  title="Please Enter the Quantity" onkeypress="return isNumberKey(event)">
                                                                <input type="hidden" name="product_qty_location_id[0]" id="product_qty_location_id0">
                                                            </td>
                                                            <td>
                                                                <select class="selectpicker form-control show-tick product_locations required" name="product_location[0]" id="product_location0"  title="Please select the location"  data-live-search="true" >
                                                                    <option value="">-- Please select --</option>
                                                                    <?php if(!empty($locations) && count($locations) > 0) { 
                                                                    foreach ($locations as $key => $value) {
                                                                    ?>
                                                                    <option value="<?php echo $value['location_id']; ?>"><?php echo $value['location_name']." [".$value['location_code']."]"; ?></option>
                                                                    <?php 
                                                                    }
                                                                    ?>
                                                                    <?php } ?>
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <select class=" form-control show-tick required" name="status[0]" id="status0">
                                                                    <option value="Active" selected = "selected">Active</option>
                                                                    <option value="Active" selected = "selected">Active</option>
                                                                    <option value="In-active">In-active</option>
                                                                </select>
                                                            </td>
    	                                        			<td>
    	                                        				<button title="Remove" class="btn-danger btn btn-sm remove">
    																<i class="material-icons">remove</i>
    															</button>
    	                                        			</td>
    	                                        		</tr>
    	                                        	<?php } ?>
    	                                        	</tbody>
    	                                        </table>
                                           </div>
                                   		</div>

                                   		<div class="panel-footer text-right">
	                                   		<button class="btn btn-sm btn-success add_more" type="button">
												<i class="fa fa-plus" style="font-size:15px;"></i> Add More
											</button>
                                    	</div>
                                    </div>
                                </div>
                            <?php if(!empty($product_locations) && count($product_locations) > 0) { 
                                ?>
                                 <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="remark" id="remark" value="<?php echo(!empty($category_data['remark']))?$category_data['remark']:"";?>" >
                                        <label class="form-label">Remark</label>
                                    </div>
                                    <div class="help-info"></div>
                                </div>
                                   <?php  } ?>
                                <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                                <a href="<?php echo base_url($this->router->fetch_module());?>" class="btn btn-primary waves-effect" type="button">Cancel</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Widgets -->
        </div>
    </section>
<script>

    function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
              && (charCode < 48 || charCode > 57)) {
                return false;
              // alert("only integer Allow");
            }
            return true;
        }



	var vRules = {
		"category_id":{required:true},
		"product_name":{required:true}
		// "product_image":{required:true}
		// "product_qty[]":{required:true},
		// "product_location[]":{required:true,minlength: 1}
	};
	var vMessages = {
		"category_id":{required:"Please select category"},
		"product_name":{required:"Please enter the product name"}
		// "product_image":{required:"Please choose image file"
		// "product_qty[]":{required:"Please Enter the product quantity"},
		// "product_location[]":{required:"Please select the product location",minlength: $.format('Please select at least {0} things.')}
	};

	$("#form-validate").validate({
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form) 
		{
			var act = "<?php echo base_url($this->router->fetch_module()); ?>/submitForm";
			$("#form-validate").ajaxSubmit({
				url: act, 
				type: 'post',
				dataType: 'json',
				cache: false,
				clearForm: false, 
				beforeSubmit : function(arr, $form, options){
					$(".btn-primary").hide();
				},
				success: function (response) 
				{
					$(".btn-primary").show();
					if(response.success)
					{
					    swal({title: "Success!",text:response.msg,confirmButtonColor: "#a81f3b",imageUrl: "<?php echo base_url('assets/images/thumbs-up.png');?>"});
						setTimeout(function(){
						window.location = "<?php echo base_url();?>product";
						},2000);
					}else{	
						swal("Problem in Changing Status!");
						return false;
					}
				}
			});
		}
	});

	$(document).ready(function(){
		var wrapper     = $(".tbody_container"); //Fields wrapper
		var add_button	= $(".add_more"); //Add button ID
		x = 0;
		$(add_button).click(function(e){ 
			var x = $("#admore_table tbody tr:last-child").attr("tr_count");
			x++;
			if(isNaN(x) || x == "undefined"){
				x = 0;
			}
			
			var html ='<tr tr_count="'+x+'" class="active">'+
						'<td>'+
                            '<input type="text" class="form-control" name="product_qty['+x+']" id="product_qty'+x+'" required="required" title="Please Enter the Quantity" onkeypress="return isNumberKey(event)">'+
                            '<input type="hidden" name="product_qty_location_id['+x+']" id="product_qty_location_id'+x+'">'+
                        '</td>'+
                        '<td>'+
                            '<select class="selectpicker form-control show-tick product_locations required"  id="product_location'+x+'" name="product_location['+x+']" title="Please select the location" >'+
                                '<option value="">-- Please select --</option>'+
                                <?php if(!empty($locations) && count($locations) > 0) { 
                                foreach ($locations as $key => $value) {
                                ?>
                                '<option value="<?php echo $value['location_id']; ?>"><?php echo $value['location_name']." [".$value['location_code']."]"; ?></option>'+
                                <?php 
                                }} ?>
                            '</select>'+
                        '</td>'+
                        '<td>'+
                            '<select class=" form-control show-tick required" name="status['+x+']" id="status'+x+'">'+
                                '<option value="Active" selected="selected">Active</option>'+
                                '<option value="Active" selected="selected">Active</option>'+
                                '<option value="In-active">In-active</option>'+
                            '</select>'+
                        '</td>'+
						'<td class="align-middle">'+
							'<button title="Remove" class="btn-danger btn btn-sm remove">'+
								'<i class="material-icons">remove</i>'+
							'</button>'+
						'</td>'+
					'</tr>';
			$(wrapper).append(html);
			$("select").selectpicker('refresh');
			// $("#companies"+x).select2({data:companies});
		});
		
		$(wrapper).on("click",".remove", function(e){ //user click on remove text
			// alert("remove");
			// alert($(this).parent('tr').attr('tr_count'));
			// alert($(this).closest('tr').attr('tr_count'));

			var tr_count = $(this).closest('tr').attr('tr_count');
			// alert(tr_count);
			e.preventDefault(); 
			if(tr_count != 0){
				$(this).closest('tr').remove(); 
				x--;
			}else{
				swal({title: "Alert!",text: "<p class='font-bold col-red'>Atleast one option is required..</p>",timer: 4000,showConfirmButton: false,html: true});
			}
		});
	});


	$('select.product_locations').on( 'hide.bs.select', function ( ) {
	    $(this).trigger("focusout");
	});

	$(document).on("changed.bs.select", "select.product_locations", function(){
		
		var $elements = $('select.product_locations');
		
		var flag = false;

		$elements.each(function () {
			
			var selectedValue = this.value;
			// alert(selectedValue);
			
			if(selectedValue != "")
			{
				$elements
				.not(this)
				.filter(function() {
					if(!flag)
					{
						if(this.value == selectedValue)
						{
							flag  = true;
							$(this).val("");
							$(this).selectpicker('refresh');
							alert('Already Selected Location..');
							return false;
						}	
					}	
				});
			}
		});
	});


function deleteFile(id,name) {

    // alert(id);
    // alert(name);
    // return

    swal({
        title: "Are you sure?",
        text: "To Delete this File.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#a81f3b",
        confirmButtonText: "Yes, Delete it !",
        closeOnConfirm: false
    }, function () {
        $.ajax({
            url: "<?php echo base_url($this->uri->segment(1))?>/deleteFile",
            async: false,
            data : { id : id,name: name},
            type: "POST",
            dataType: "json",
            success: function (response){
                if(response.success){
                    swal({title: "Success!",text: "File Deleted Successfully.",confirmButtonColor: "#a81f3b",imageUrl: "<?php echo base_url('assets/images/thumbs-up.png');?>"});
                    setTimeout(function(){
                        window.location = "<?php echo base_url($this->router->fetch_module());?>";
                    },2000);
                }else{
                    swal("Problem in Deleting File!");
                    return false;
                }
            }
        });
        
    });
}
</script>