<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Login extends CI_Controller 
{	
	function __construct(){
		parent::__construct();
		$this->load->model('loginmodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
		if(!empty($_SESSION["jain_decorator_admin"])){
			redirect("home");
		}
	}
	function index(){
		$this->load->view('template/login_header.php');
		$this->load->view('login/index');
		$this->load->view('template/footer.php');
	}
	function loginvalidate(){
		$result = $this->common->getData("tbl_users","*",array("username"=>$_POST['username'],"password"=>md5($_POST['password']),"status"=>"In-active"));
		if($result){
			$_SESSION["jain_decorator_admin"] = $result;
			echo json_encode(array("success"=>true, "msg"=>'You are successfully logged in.'));
			exit;		
		}else{
			echo json_encode(array("success"=>false, "msg"=>'Username or Password incorrect.'));
			exit;
		}
	}
}

?>
