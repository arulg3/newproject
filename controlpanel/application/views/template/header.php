<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<title>WWMEATS</title>
		<!-- Favicon-->
		<!-- Google Fonts -->
	<!--------------------------------CSS START----------------------------------->
		<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
		<!-- Bootstrap Core Css -->
		<link href="<?php echo base_url();?>assets/js/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
		<!-- Waves Effect Css -->
		<link href="<?php echo base_url();?>assets/js/plugins/node-waves/waves.css" rel="stylesheet" />
		<!-- Animation Css -->
            <link href="<?php echo base_url();?>assets/css/lightbox.min.css" rel="stylesheet" />
        <!--  for light box-->
		<link href="<?php echo base_url();?>assets/js/plugins/animate-css/animate.css" rel="stylesheet" />
		<!-- Morris Chart Css-->
		<link href="<?php echo base_url();?>assets/js/plugins/morrisjs/morris.css" rel="stylesheet" />
		
		<!-- sweet alert Css-->
		<link href="<?php echo base_url();?>assets/js/plugins/sweetalert/sweetalert.css" rel="stylesheet" />
		
		<!-- Custom Css -->
		<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
		<link href="<?php echo base_url();?>assets/css/themes/all-themes.css" rel="stylesheet" />
		
		<!---datatable-->
		<link href="<?PHP echo base_url();?>assets/js/plugins/data_table/css/jquery.dataTables.css" rel="stylesheet">
		<link href="<?PHP echo base_url();?>assets/js/plugins/data_table/css/rowReorder.dataTables.min.css" rel="stylesheet">
		<link href="<?PHP echo base_url();?>assets/js/plugins/data_table/css/responsive.dataTables.min.css" rel="stylesheet">
		
		<!---selectbox-->
		<link href="<?PHP echo base_url();?>assets/js/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet">

        <!--Datepicker CSS--->
        <link href='<?PHP echo base_url();?>assets/js/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css'>

        <!--Daterangepicker CSS--->
        <link href='<?PHP echo base_url();?>assets/js/plugins/bootstrap-daterangepicker/css/daterangepicker.css'>

<!--------------------------------CSS END----------------------------------->

<!--------------------------------script start----------------------------------->
		  <!-- Jquery Core Js -->
		<script src="<?php echo base_url();?>assets/js/plugins/jquery/jquery.min.js"></script>
		
        <script src="<?php echo base_url();?>assets/js/lightbox.js"></script>

		<!-- Bootstrap Core Js -->
		<script src="<?php echo base_url();?>assets/js/plugins/bootstrap/js/bootstrap.js"></script>
		
		<!-- Select Plugin Js -->
		<script src="<?php echo base_url();?>assets/js/plugins/bootstrap-select/js/bootstrap-select.js"></script>
		
		<!-- Slimscroll Plugin Js -->
		<script src="<?php echo base_url();?>assets/js/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
		
		<!-- Waves Effect Plugin Js -->
		<script src="<?php echo base_url();?>assets/js/plugins/node-waves/waves.js"></script>
		
		<!-- SWEET ALERT Plugin Js -->
		<script src="<?php echo base_url();?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/plugins/sweetalert/sweetalert-dev.js"></script>
        		
		<!-- Custom Js -->
		<script src="<?php echo base_url();?>assets/js/admin.js"></script>
		
		<!-- Demo Js -->
		<script src="<?php echo base_url();?>assets/js/demo.js"></script>
		
		<!--Data table Js-->
		<script src='<?PHP echo base_url();?>assets/js/plugins/data_table/js/jquery.dataTables.min.js'></script>
		<script src='<?PHP echo base_url();?>assets/js/plugins/data_table/js/datatable.js'></script>
		<script src='<?PHP echo base_url();?>assets/js/plugins/data_table/js/dataTables.rowReorder.min.js'></script>
		<script src='<?PHP echo base_url();?>assets/js/plugins/data_table/js/dataTables.responsive.min.js'></script>
		
		<!--Form Validate--->
		<script src='<?PHP echo base_url();?>assets/js/plugins/jquery-validation/jquery.validate.js'></script>
		<script src='<?PHP echo base_url();?>assets/js/plugins/jquery-validation/additional-methods.js'></script>
        <script src='<?PHP echo base_url();?>assets/js/plugins/jquery-validation/form-validation.js'></script>
        <script src='<?PHP echo base_url();?>assets/js/plugins/jquery-validation/jquery.form.js'></script>

        <!--Moment JS--->
        <script src='<?PHP echo base_url();?>assets/js/plugins/momentjs/moment.js'></script>

        <!--Datepicker JS--->
        <script src='<?PHP echo base_url();?>assets/js/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js'></script>

        <!--Daterangepicker JS--->
        <script src='<?PHP echo base_url();?>assets/js/plugins/bootstrap-daterangepicker/js/daterangepicker.js'></script>
        <script src='<?PHP echo base_url();?>assets/js/plugins/ckeditor/ckeditor.js'></script>
        <!-- Tooltip -->
        <script>
        $(function () {
            //Tooltip
            $('[data-toggle="tooltip"]').tooltip({
                container: 'body'
            });
            //Popover
            $('[data-toggle="popover"]').popover();
        });
        </script>
<!--------------------------------script End----------------------------------->
	</head>
    <?php 
        $login_class = "";
        $theme_class ="theme-white";
        if(($this->uri->segment(1)=="login" || $this->uri->segment(1)=="") && empty($_SESSION['jain_decorators_admin'])){
            $login_class = "display_none";
            $theme_class ="login-page";
        }
    ?>
    <div class ="<?php echo $login_class;?>">
    	<nav class="navbar ">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a href="javascript:void(0);" class="bars"></a>
                    <a class="navbar-brand text-center" href="<?php echo base_url("home");?>">
                        <!-- <img src="<?php echo base_url("assets/images/logo.png");?>" class="img-responsive"> -->
                        <h2>WWMEATS</h2>
                    </a>
                </div>
            </div>
        </nav>
    	<section class="<?php echo $login_class;?>">
            <!-- Left Sidebar -->
            <aside id="leftsidebar" class="sidebar">
                <div class="menu">
                    <ul class="list">
                        <li class="<?php echo($this->uri->segment(1) == "home" || $this->uri->segment(1) =="")?"active":"";?>" style="padding:10px 0px;border-bottom:1px solid #43000b;">
                            <div class="col-sm-3 col-xs-3">
                                <img src="<?php echo base_url("assets/images/user.png")?>" width="48" height="48" alt="User" width="50px" />
                            </div>
                            <div class="col-sm-6 col-xs-6 btn-group btn-group-username">
                                User Name
                            </div>
                            <div class="col-sm-3 col-xs-3"  width="50px">
                                <div class="btn-group user-helper-dropdown ">
                                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                                    <ul class="dropdown-menu pull-right arrow_box">
                                        <li><a href="<?php echo base_url("profile")?>"><i class="material-icons">person</i>Profile</a></li>
                                        <li role="separator" class="divider"></li>
                                        <li><a href="<?php echo base_url("home/logout")?>"><i class="material-icons">input</i>Sign Out</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </li>
                        <li>
                            <a href="javascript:void(0);" class="menu-toggle <?php echo(in_array($this->uri->segment(1), array("contents","website_pages")))?"toggled":"";?>">
                                <i class="material-icons">apps</i>
                                <span>CMS</span>
                            </a>
                            <ul class="ml-menu">
                                <li class="<?php echo($this->uri->segment(1) == "contents" || $this->uri->segment(1) =="")?"active":"";?>">
                                    <a href="<?php echo base_url("contents")?>">
                                        <i class="material-icons">ballot</i>
                                        <span>Contents</span>
                                    </a>
                                </li>
                                <li class="<?php echo($this->uri->segment(1) == "website_pages" || $this->uri->segment(1) =="")?"active":"";?>">
                                    <a href="<?php echo base_url("website_pages")?>">
                                        <i class="material-icons">desktop_windows</i>
                                        <span>Website Pages</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </aside>
            <!-- #END# Left Sidebar -->
        </section>
    </div>
	<body class="<?php echo $theme_class;?>">