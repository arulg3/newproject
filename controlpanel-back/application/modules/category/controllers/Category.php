<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Category extends CI_Controller 
{	
	function __construct(){
		parent::__construct();
		$this->load->model('categorymodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
		checklogin();
	}
 
	function index(){
		$result['category'] = $this->common->getData("tbl_category","*",'');
		$this->load->view('template/header.php');
		$this->load->view('category/index',$result);
		$this->load->view('template/footer.php');
		// echo "<pre>";
		// print_r($result);
		// exit();
	}
	
	function addEdit(){
		$record_id = "";
		$edit_datas = array();
		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$record_id = $url_prams['id'];
			$result = $this->common->getData("tbl_category","*",array("category_id"=>$record_id));
			if(!empty($result)){
				$edit_datas['category_data'] = $result[0];
			}
		}
		$this->load->view('template/header.php');
		$this->load->view('category/addEdit',$edit_datas);
		$this->load->view('template/footer.php');
	}
	function fetch(){
		$get_result = $this->categorymodel->getRecords($_GET);
		$result = array();
		$result["sEcho"]= $_GET['sEcho'];
		$result["iTotalRecords"] = $get_result['totalRecords'];	//iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"]= $get_result['totalRecords']; //iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result']) && count($get_result['query_result']) > 0){
			for($i=0;$i<sizeof($get_result['query_result']);$i++){
				$temp = array();
				array_push($temp, ucfirst($get_result['query_result'][$i]->category_name));
				
				$image = "No Image";
				if(!empty($get_result['query_result'][$i]->category_image)){					
					$image = "<img src='".base_url("uploads/category_images/".$get_result['query_result'][$i]->category_image)."' class='preview_uploaded_image'>";
				}
				array_push($temp,$image);
				
				array_push($temp, $get_result['query_result'][$i]->status);
				$status_change = "";
				$actionCol = "";
				$status_type = '';
				if($get_result['query_result'][$i]->status == 'Active'){
					$status_type = "checked = 'checked'";
				}
				
				$status_change = '<div class="switch">
												<label>
													<input type="checkbox" class="status_change" onchange="changeStatus('.$get_result['query_result'][$i]->category_id.',this)"  '.$status_type.'">
													<span class="lever"></span>
												</label>
											</div>';
				array_push($temp, $status_change);
				
				$actionCol .='<a href="category/addEdit?text='.rtrim(strtr(base64_encode("id=".$get_result['query_result'][$i]->category_id), '+/', '-_'), '=').'" title="Edit" class="text-center"><i class="material-icons ">create</i></a>';
				array_push($temp, $actionCol);
				
				array_push($items, $temp);
			}
		}
		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}
	
	function submitForm(){
		// print_r($_FILES);exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{

			// echo "<pre>";
			// print_r($_POST);
			// exit;

			/*check duplicate entry*/	
			$condition = "category_name = '".$this->input->post('category_name')."' ";
			if(!empty($this->input->post("category_id"))){
				$condition .= " AND category_id <> ".$this->input->post("category_id");
			}	

			$chk_category_sql = $this->common->Fetch("tbl_category","category_id",$condition);
			$rs_category = $this->common->MySqlFetchRow($chk_category_sql, "array");

			if(!empty($rs_category[0]['category_id'])){
				echo json_encode(array('success'=>false, 'msg'=>'Category name already exist...'));
				exit;
			}

			$data = array();
			$data['category_name'] = $this->input->post('category_name');
			
			/* image Upload */
			if(!empty($_FILES['category_image'])){
				$path = DOC_ROOT_FRONT.'/uploads/category_images/';
				if(!empty($this->input->post('existing_category_image'))){
					$img_path = $path."/".$this->input->post('existing_category_image');
					if(is_file($img_path)){
						unlink($img_path);
					}
				}
				$this->load->library('upload');
				$this->upload->initialize($this->set_upload_options());
                if (!$this->upload->do_upload('category_image')){
					$error = array('error' => $this->upload->display_errors());
					echo json_encode(array('success'=>false, 'msg'=>$error));
					exit;
                }else{
					$file_data = array('upload_data' => $this->upload->data());
					$data['category_image'] = $file_data['upload_data']['file_name'];
                }
			}
			/* image Upload */

			if(!empty($this->input->post("category_id"))){
				
				$data['updated_by'] = $_SESSION['jain_decorator_admin'][0]['user_id'];
				$data['updated_on'] = date("Y-m-d H:i:s");

				$result = $this->common->updateData("tbl_category",$data,array("category_id"=>$this->input->post("category_id")));
				if($result){
					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while Updating data.'));
					exit;
				}
			}else{
				
				$data['created_by'] = $_SESSION['jain_decorator_admin'][0]['user_id'];
				$data['created_on'] = date("Y-m-d H:i:s");

				$result = $this->common->insertData("tbl_category",$data,"1");
				if($result){
					echo json_encode(array('success'=>true, 'msg'=>'Record Inserted Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while Inserting data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem while add/edit data.'));
			exit;
		}
	}
	
	function changeStatus(){
		if(!empty($this->input->post("id"))){
			$existing_data = $this->common->getData("tbl_category","status",array('category_id'=>$this->input->post("id")));
			$data = array();
			if(!empty($existing_data) && $existing_data[0]['status'] == "Active"){
				$data['status'] = "In-active";
			}else{
				$data['status'] = "Active";
			}
			$result = $this->common->updateData("tbl_category",$data,array("category_id"=>$this->input->post("id")));
			if($result){
				echo json_encode(array('success'=>true));
				exit;
			}else{
				echo json_encode(array('success'=>false));
			}
		}
	}

	function dataExist(){
		$condition = array("category_name"=>$_POST['category_name']);

		if(!empty($_POST['category_id']) && $_POST['category_id'] !=""){
			$condition['category_id <>'] = $_POST['category_id'];
		}

		$result=$this->common->getData("tbl_category","*",$condition);
				
		// echo $this->db->last_query();
		// echo "<pre>";
		// print_r($result);
		// exit;
				
		if($result > 0){
			echo  json_encode(FALSE);
		}else{
			echo  json_encode(TRUE);
		} 
	}
	
	function set_upload_options($file_name = ""){   
		$config = array();
		if(!empty($file_name) &&  $file_name != ""){
			$config['file_name']   = $file_name;
		}
		$config['upload_path'] = DOC_ROOT_FRONT ."/uploads/category_images/";
		if(!is_dir($config['upload_path'])){
			mkdir($config['upload_path'],0777, true);		
		}
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size']      = '0';
		$config['overwrite']     = FALSE;
		return $config;
	} 
	
	function deleteFile(){
		$path = DOC_ROOT_FRONT.'/uploads/category_images/';
		if(!empty($this->input->post('name'))){
			$data = array();
			$img_path = $path."/".$this->input->post('name');
			if(is_file($img_path)){
				unlink($img_path);
			}
			$data['category_image'] = "";
			$result = $this->common->updateData("tbl_category",$data,array("category_id"=>$this->input->post("id")));
			if($result){
				echo json_encode(array('success'=>true));
				exit;
			}else{
				echo json_encode(array('success'=>false));
			}
		}
	}
	
}

?>
