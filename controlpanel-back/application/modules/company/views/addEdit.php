<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD</h2>
            </div>
            <!-- Widgets -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="body">
						<ol class="breadcrumb align-right">
							<li><a href="<?php echo base_url("home");?>"><i class="material-icons">home</i> Home</a></li>
							<li><a href="<?php echo base_url($this->router->fetch_module());?>"><i class="material-icons">local_shipping</i>Transport</a></li>
							<li class="active"><i class="material-icons">mode_edit</i> AddEdit</li>
						</ol>
					</div>
                    <div class="card">
                        <div class="header">
                            <h2>Add / Edit Transport</h2>
                        </div>
                        <div class="body">
                            <form id="form-validate" class="form-validate" name="form-validate" method="POST">
								<input type="hidden" name="company_id" id="company_id" value="<?php echo(!empty($company_data['company_id']))?$company_data['company_id']:"";?>">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="company_name" id="company_name" value="<?php echo(!empty($company_data['company_name']))?$company_data['company_name']:"";?>" required>
                                        <label class="form-label">Transport Name</label>
                                    </div>
                                    <div class="help-info"></div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <textarea class="form-control" name="company_address" id="company_address" required><?php echo(!empty($company_data['company_address']))?$company_data['company_address']:"";?></textarea>
                                        <label class="form-label">Transport Address</label>
                                    </div>
                                    <div class="help-info"></div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="company_spoc_name" id="company_spoc_name" value="<?php echo(!empty($company_data['company_spoc_name']))?$company_data['company_spoc_name']:"";?>" required>
                                        <label class="form-label">Transport SPOC Name</label>
                                    </div>
                                    <div class="help-info"></div>
                                </div>
								<div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="company_spoc_phone_number" id="company_spoc_phone_number" value="<?php echo(!empty($company_data['company_spoc_phone_number']))?$company_data['company_spoc_phone_number']:"";?>" maxlength="10" required>
                                        <label class="form-label">Transport SPOC Phone Number</label>
                                    </div>
                                    <div class="help-info"></div>
                                </div>
                                <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                                <a href="<?php echo base_url($this->router->fetch_module());?>" class="btn btn-primary waves-effect" type="button">Cancel</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Widgets -->
        </div>
    </section>
<script>
	var vRules = {
		"company_name":{required:true,remote:{
						url:"<?php echo base_url($this->router->fetch_module().'/dataCompanynameExist');?>" ,
						type: "post",
						data: {company_name: function() {return $( "#company_name" ).val();},company_id:function() {return $( "#company_id" ).val();}}
					  }},
		"company_address":{required:true},
		"company_spoc_name":{required:true},
		"company_spoc_phone_number":{required:true,digits:true}
	};
	var vMessages = {
		"company_name":{required:"Please Enter the Transport Name"},
		"company_address":{required:"Please Enter the Transport Address"},
		"company_spoc_name":{required:"Please Enter the Transport SPOC Name"},
		"company_spoc_phone_number":{required:"Please Enter the Transport SPOC Phone Number",digits:"Please enter valid phone no."}
	};

	$("#form-validate").validate({
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form) 
		{
			var act = "<?php echo base_url($this->router->fetch_module()); ?>/submitForm";
			$("#form-validate").ajaxSubmit({
				url: act, 
				type: 'post',
				dataType: 'json',
				cache: false,
				clearForm: false, 
				beforeSubmit : function(arr, $form, options){
					$(".btn-primary").hide();
				},
				success: function (response) 
				{
					$(".btn-primary").show();
					if(response.success)
					{
					    swal({title: "Success!",text:response.msg,confirmButtonColor: "#a81f3b",imageUrl: "<?php echo base_url('assets/images/thumbs-up.png');?>"});
						setTimeout(function(){
							window.location = "<?php echo base_url($this->router->fetch_module());?>";
						},2000);
					}else{	
						swal(response.msg);
						return false;
					}
				}
			});
		}
	});
</script>