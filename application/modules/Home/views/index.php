


<div class="container-fluid">
	<div class="row">
		<div class="home-banners-slider">
			<div class="item">
				<img src="<?php echo base_url("assets/images/home-banner2.jpg")?>" alt="">
				<div class="slide-caption">
					<h2 class="slide-caption__title">Welcome to WWMeats</h2>
					<p class="slide-caption__desc"> Fresh and Clean Meat. </p>
					<!-- <a href="#" class="btn">Mussum ipsum button</a> -->
				</div>
			</div>
			<div class="item">
				<img src="<?php echo base_url("assets/images/home-banner3.jpg")?>" alt="">
				<div class="slide-caption">
					<h2 class="slide-caption__title">Welcome to WWMeats</h2>
					<p class="slide-caption__desc"> Fresh and Clean Meat.</p>
					<!-- <a href="#" class="btn">Mussum ipsum button</a> -->
				</div>
			</div>
		</div>
	</div>
</div>
<div class="login-boxes text-center"  data-aos="zoom-in"  data-aos-offset="-300">
	<div class="login-boxes-wrapper">
		<div class="col-sm-6 col-md-6 col-xs-12">
			<div class="exporter-login-box">
				<button class="btn btn-primary"  data-toggle="modal" data-target=".exporter-login-register-form">
					LOGIN AS EXPORTER
				</button>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="col-sm-6 col-md-6 col-xs-12">
			<div class=" buyer-login-box ">
				<button class="btn btn-primary"  data-toggle="modal" data-target=".buyer-login-register-form">
					LOGIN AS BUYER
				</button> 
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div class="clearfix"></div>


<div class="promo-area" >
	<div class="container" >
		<h2 class="section-title ">Instant Support</h2>
		<div class="row">
			<div class="col-md-2 col-sm-4 col-xs-4" data-aos="zoom-in" data-aos-offset="-200">
				<div class="single-promo">
					<img src="<?php echo base_url("assets/images/whatsapp_icon.png")?>" alt="">
				</div>
				<div class="single-promo-title text-center">
					Whatsapp
				</div>
			</div>
			<div class="col-md-2 col-sm-4 col-xs-4" data-aos="zoom-in"  data-aos-offset="-200">
				<div class="single-promo">
					<img src="<?php echo base_url("assets/images/line_icon.png")?>" alt="">
				</div>
				<div class="single-promo-title text-center">
					Line Chat
				</div>
			</div>
			<div class="col-md-2 col-sm-4 col-xs-4" data-aos="zoom-in"  data-aos-offset="-200">
				<div class="single-promo">
					<img src="<?php echo base_url("assets/images/wechat_icon.png")?>" alt="">
				</div>
				<div class="single-promo-title text-center">
					We Chat
				</div>
			</div>
			<div class="col-md-2 col-sm-4 col-xs-4" data-aos="zoom-in"  data-aos-offset="-200">
				<div class="single-promo">
					<img src="<?php echo base_url("assets/images/message_icon.png")?>" alt="">
				</div>
				<div class="single-promo-title text-center">
					Text Message 
				</div>
			</div>
			<div class="col-md-2 col-sm-4 col-xs-4" data-aos="zoom-in"  data-aos-offset="-200">
				<div class="single-promo">
					<img src="<?php echo base_url("assets/images/phone_icon.png")?>" alt="">
				</div>
				<div class="single-promo-title text-center">
					Phone
				</div>
			</div>
			<div class="col-md-2 col-sm-4 col-xs-4" data-aos="zoom-in"  data-aos-offset="-200">
				<div class="single-promo">
					<img src="<?php echo base_url("assets/images/email_icon.png")?>" alt="">
				</div>
				<div class="single-promo-title text-center">
					Email
				</div>
			</div>
		</div>
	</div>
</div>
<!-- End promo area -->
<div class="maincontent-area" data-aos="fade-up" data-aos-offset="-100">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="latest-product">
					<h2 class="section-title">Categories</h2>
					<div class="product-carousel">
						<div class="single-product shsdow-effect" >
							<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 pull-right single-product-image" >
								<img src="<?php echo base_url("assets/images/sample.jpg")?>" alt="">
							</div>
							<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 single-product-description" >
								<h1>
									Product Name
								</h1>
								<p>
								Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
								</p>
								<button class="btn btn-primary">
									View Products <i class="fa fa-arrow-right"></i>
								</button>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="single-product shsdow-effect">
							<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 single-product-image" >
								<img src="<?php echo base_url("assets/images/sample.jpg")?>" alt="">
							</div>
							<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 single-product-description" >
								<h1>
									Product Name
								</h1>
								<p>
								Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
								</p>
								<button class="btn btn-primary">
									View Products <i class="fa fa-arrow-right"></i>
								</button>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="single-product shsdow-effect">
							<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 pull-right single-product-image" >
								<img src="<?php echo base_url("assets/images/sample.jpg")?>" alt="">
							</div>
							<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 single-product-description" >
								<h1>
									Product Name
								</h1>
								<p>
								Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
								</p>
								<button class="btn btn-primary">
									View Products <i class="fa fa-arrow-right"></i>
								</button>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="single-product shsdow-effect">
							<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 single-product-image" >
								<img src="<?php echo base_url("assets/images/sample.jpg")?>" alt="">
							</div>
							<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 single-product-description" >
								<h1>
									Product Name
								</h1>
								<p>
								Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
								</p>
								<button class="btn btn-primary">
									View Products <i class="fa fa-arrow-right"></i>
								</button>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="welcome-area" >
	<div class="container">
		<h2 class="section-title">Welcome to World Wide Meats
		</h2>
		<div class="col-sm-12" >
			<div class="row">
				<div class="col-md-4 col-sm-6 col-xs-12 welcome-grid " >
					<div class="grid-wrapper">
						<div class="welcome-grid-img">
							<img src="<?php echo base_url("assets/images/thumb-img.jpg")?>" alt="">
						</div>
						<div class=" text-left welcome-grid-article">
							<h3>Lorem Ipsum</h3>
							<p>
								Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
							</p>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12 welcome-grid" >
					<div class="grid-wrapper">
						<div class="welcome-grid-img">
							<img src="<?php echo base_url("assets/images/thumb-img.jpg")?>" alt="">
						</div>
						<div class=" text-left welcome-grid-article">
							<h3>Lorem Ipsum</h3>
							<p>
								Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
							</p>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12 welcome-grid" >
					<div class="grid-wrapper">
						<div class="welcome-grid-img">
							<img src="<?php echo base_url("assets/images/thumb-img.jpg")?>" alt="">
						</div>
						<div class=" text-left welcome-grid-article">
							<h3>Lorem Ipsum</h3>
							<p>
								Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
							</p>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- End main content area -->

<script>
$(document).ready(function(){
	var left_arrow = "<?php echo base_url('assets/images/left-arrow.png'); ?>";
	var right_arrow = "<?php echo base_url('assets/images/right-arrow.png'); ?>";
	$(".home-banners-slider").owlCarousel({
		items: 1,
		loop: true,
		autoplay: false,
		autoplayTimeout: 2000,
		nav: true,
		dots: false,
		navText: ['<img src="'+left_arrow+'" class="slider-arrow-cls">','<img src="'+right_arrow+'" class="slider-arrow-cls">'],
	});  
	$(".home-banners-slider").on('changed.owl.carousel', function(event) {
		$('.owl-item').find('h1').addClass('animated bounce');
	});


	$(".product-carousel").owlCarousel({
		items: 1,
		loop: true,
		autoplay: true,
		autoplayTimeout: 5000,
		nav: true,
		dots: true,
		navText: ['<img src="'+left_arrow+'" class="slider-arrow-cls">','<img src="'+right_arrow+'" class="slider-arrow-cls">'],
	});  

	
});  
</script>
<style>

</style>
<!-- <input id="phone" name="phone" type="tel">
<script>
	$(document).ready(function(){
		var input = document.querySelector("#phone");
		window.intlTelInput(input, {
			preferredCountries: ['in','cn','th']
		});
	})
  </script> -->
  <style>
  
  </style>