<?php
if (!defined('BASEPATH'))
exit('No direct script access allowed');
class Register extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('registermodel', '', TRUE);
        $this->load->model('common_model/common_model', 'common', TRUE);
    }
    function index()
    {
        if(!empty($_GET['user']) && ($_GET['user'] == "exporter" || $_GET['user'] == "buyer")){
            $data = array();
            $data['register_uer'] = $_GET['user'];
            $this->load->view('templates/header.php');
            $this->load->view('Register/index',$data);
            $this->load->view('templates/footer.php');
        }else{
            redirect('/Home', 'refresh');
        }
    }

    function submitForm(){
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            if(!empty($this->input->post('user_type')) && $this->input->post('user_type') == "exporter"){
                $data = array();
                $data['exporter_company_name'] = $this->input->post('exporter_company_name');
                $data['exporter_phone_number'] = $this->input->post('exporter_phone_number');
                $data['status'] = "In-active";
                /* image Upload */
                if(!empty($_FILES['apeda_file'])){
                    $this->load->library('upload');
                    $this->upload->initialize($this->set_upload_options());
                    if (!$this->upload->do_upload('apeda_file')){
                        $error = array('error' => $this->upload->display_errors());
                        echo json_encode(array('success'=>false, 'msg'=>$error));
                        exit;
                    }else{
                        $file_data = array('upload_data' => $this->upload->data());
                        $data['apeda_file'] = $file_data['upload_data']['file_name'];
                    }
                }
                /* image Upload */
                $exporter_id = $this->common->insertData("tbl_exporter",$data);
                if($exporter_id){
                    // insert data into admin   
                    $adminData = array();
                    $adminData['username'] = $this->input->post('username');
                    $adminData['user_relation_id'] = $exporter_id;
                    $adminData['user_type'] = 3;
                    $adminData['status'] = "In-active";
                    $adminData['password'] = md5($this->input->post('password'));
                    $result = $this->common->insertData("tbl_users",$adminData);
                    if($result){
                        echo json_encode(array('success'=>true, 'msg'=>'You have registered Successfully.'));
                        exit;
                    }else{
                        echo json_encode(array('success'=>false, 'msg'=>'Problem while Registration data.'));
                        exit;
                    }
                }else{
                    echo json_encode(array('success'=>false, 'msg'=>'Problem while Registration data.'));
                    exit;
                }
            }else{
                $data = array();
                $data['buyer_name'] = $this->input->post('buyer_name');
                $data['buyer_company_name'] = $this->input->post('buyer_company_name');
                $data['buyer_phone_number'] = $this->input->post('buyer_phone_number');
                $data['status'] = "Active";
                $buyer_id = $this->common->insertData("tbl_buyer",$data);
                if($buyer_id){
                    // insert data into admin   
                    $adminData = array();
                    $adminData['username'] = $this->input->post('username');
                    $adminData['user_relation_id'] = $buyer_id;
                    $adminData['user_type'] = 4;
                    $adminData['status'] = "Active";
                    $adminData['password'] = md5($this->input->post('password'));
                    $result = $this->common->insertData("tbl_users",$adminData);
                    if($result){
                        echo json_encode(array('success'=>true, 'msg'=>'You have registered Successfully.'));
                        exit;
                    }else{
                        echo json_encode(array('success'=>false, 'msg'=>'Problem while Registration data.'));
                        exit;
                    }
                }else{
                    echo json_encode(array('success'=>false, 'msg'=>'Problem while Registration data.'));
                    exit;
                }
            }
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem while add/edit data.'));
			exit;
		}
    }

    function set_upload_options($file_name = ""){   
		$config = array();
		if(!empty($file_name) &&  $file_name != ""){
			$config['file_name']   = $file_name;
		}
		$config['upload_path'] = DOC_ROOT_FRONT ."/uploads/apeda_files/";
		if(!is_dir($config['upload_path'])){
			mkdir($config['upload_path'],0777, true);		
		}
		$config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
		$config['max_size']      = '0';
		$config['overwrite']     = FALSE;
		return $config;
    }
    
    function checkExisting(){
        if(!empty($_POST)){
            if(!empty($_POST['username'])){
                $condition = "username = '".$_POST['username']."' ";
                if(!empty($_POST) && $_POST['user_type'] == "exporter"){
                    $condition .= " AND user_type = 3";
                }else{
                    $condition .= " AND user_type = 4";
                }
                $result = $this->common->getData("tbl_users","*",$condition);
                if(!empty($result)){
                    echo  json_encode(FALSE);
                }else{
                    echo  json_encode(TRUE);
                }
            }   
        }
    }

    function loginValidate(){
		$result = $this->common->getData("tbl_users","*",array("username"=>$_POST['username'],"password"=>md5($_POST['password']),"user_type"=>$_POST['user_type'],"status"=>"Active "));
		if($result){
			$_SESSION["wwmeat_user"] = $result;
			echo json_encode(array("success"=>true, "msg"=>'You are successfully logged in.'));
			exit;		
		}else{
			echo json_encode(array("success"=>false, "msg"=>'Username or Password incorrect.'));
			exit;
		}
    }
    
    function logout(){
        unset($_SESSION['wwmeat_user']);
        redirect("/home","refresh");
    }
}
?>
