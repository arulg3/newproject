<div class="container-fluid p0">
	<div class="row page-banner">
		<img src="<?php echo base_url("assets/images/banner2.jpg")?>" alt="" class="image-responsive">
	</div>
	<div class="page-banner-title text-center mb-30 mt-30">
		<h1>About Us</h1>
	</div>
	<div class="about-us-container container-fluid mt-30">
		<div class="zigzag-content">
			<div class="col-sm-12 col-md-12 zig-zag-wrapper">
				<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 zig pull-right">
					<div class="row">
						<img src="<?php echo base_url("assets/images/thumb-img.jpg")?>" alt="">
					</div>
				</div>
				<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 zag ">
					<h2 class=" text-left">How We Work</h2>
					<p>
						Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. 
					</p>
				</div>
			</div>
			<div class="col-sm-12 col-md-12 zig-zag-wrapper">
				<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 zig">
					<div class="row">
						<img src="<?php echo base_url("assets/images/thumb-img.jpg")?>" alt="">
					</div>
				</div>
				<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 zag">
					<h2 class=" text-left">How We Work</h2>
					<p>
						Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. 
					</p>
				</div>
			</div>
			<div class="col-sm-12 col-md-12 zig-zag-wrapper">
				<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 zig pull-right">
					<div class="row">
						<img src="<?php echo base_url("assets/images/thumb-img.jpg")?>" alt="">
					</div>
				</div>
				<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 zag">
					<h2 class=" text-left">How We Work</h2>
					<p>
						Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. 
					</p>
				</div>
			</div>
			<div class="col-sm-12 col-md-12 zig-zag-wrapper">
				<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 zig ">
					<div class="row">
						<img src="<?php echo base_url("assets/images/thumb-img.jpg")?>" alt="">
					</div>
				</div>
				<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 zag ">
					<h2 class=" text-left">How We Work</h2>
					<p>
						Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. 
					</p>
				</div>
			</div>
			<div class="col-sm-12 col-md-12 zig-zag-wrapper">
				<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 zig pull-right">
					<div class="row">
						<img src="<?php echo base_url("assets/images/thumb-img.jpg")?>" alt="">
					</div>
				</div>
				<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 zag">
					<h2 class=" text-left">How We Work</h2>
					<p>
						Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. 
					</p>
				</div>
			</div>
		</div>
	</div>
</div>