<?PHP
class Productmodel extends CI_Model
{
	function getRecords($get){
		// print_r($get);
		$table = "tbl_product";
		$table_id = 'product_id';
		$default_sort_column = 'product_id';
		$default_sort_order = 'desc';
		$condition = "1=1";

		$colArray = array('i.category_id','','i.product_id','i.product_size','i.status');
		$sortArray = array('c.category_name','i.product_name','i.product_size','i.status');
		
		$page = $get['iDisplayStart'];	// iDisplayStart starting offset of limit funciton
		$rows = $get['iDisplayLength'];	// iDisplayLength no of records from the offset
		
		// sort order by column
		$sort = isset($get['iSortCol_0']) ? strval($sortArray[$get['iSortCol_0']]) : $default_sort_column;  
		$order = isset($get['sSortDir_0']) ? strval($get['sSortDir_0']) : $default_sort_order;

		for($i=0;$i<4;$i++){
			if(isset($get['sSearch_'.$i]) && $get['sSearch_'.$i]!='')
			{
			    if($i == 0 || $i == 1 || $i == 3){
			       $condition .= " AND ".$colArray[$i]." = '".$_GET['Searchkey_'.$i]."' ";
			    }else{
			        $condition .= " AND ".$colArray[$i]." like '%".$_GET['Searchkey_'.$i]."%' ";
			    }
			}
		}
		
		// echo $condition	;
		// $this -> db -> select('i.*, c.category_name,tpcq.sum(product_qty) from tbl_product_current_qty group by tpcq.product_id');
		
		$this -> db -> select('i.*, c.category_name');
		$this -> db -> from("$table as i");
		$this -> db -> join("tbl_category as c", "c.category_id = i.category_id", "left");
		$this->db->where("($condition)");
		$this->db->order_by($sort, $order);
		$this->db->limit($rows,$page);
		
		$query = $this -> db -> get();
		
		// echo $this->db->last_query();
		// exit;
		
		$this -> db -> select('i.*, c.category_name');
		$this -> db -> from("$table as i");
		$this -> db -> join("tbl_category as c", "c.category_id = i.category_id", "left");
		$this->db->where("($condition)");
		$this->db->order_by($sort, $order);
		
		$query1 = $this -> db -> get();
		
		if($query -> num_rows() > 0){
			$totcount = $query1 -> num_rows();
			return array("query_result" => $query->result(), "totalRecords" => $totcount);
		}else{
			return array("totalRecords" => 0);
		}
	}

	
}
?>
