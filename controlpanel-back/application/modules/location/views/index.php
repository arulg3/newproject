<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2>Location</h2>
		</div>
		<!-- Widgets -->
		<!-- Widgets -->
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="body">
					<ol class="breadcrumb align-right">
						<li><a href="<?php echo base_url("home");?>"><i class="material-icons">home</i> Home</a></li>
						<li class="active"><i class="material-icons">location_on</i>Location</li>
					</ol>
				</div>
				<div class="card">
					<div class="header text-right">
						<h2 class="pull-left">Location</h2>
						<button class="btn btn-primary" data-toggle="collapse" data-target="#serchfilter">Filter</button>
						<a href="<?php echo base_url($this->router->fetch_module());?>/addEdit" class="btn btn-primary waves-effect">Add Location</a>
					</div>
					<div class="panel panel-default search-panel" >
						<div class="panel-body collapse" id="serchfilter">
							<div class="col-md-3 col-sm-13 col-lg-3 col-xs-12 dataTables_filter">
								<label>Location Name</label>
								<input id="sSearch_0" name="sSearch_0" type="text" class="searchInput  form-control">
							</div>
							<div class="col-md-3 col-sm-13 col-lg-3 col-xs-12 dataTables_filter">
								<label>Location Code</label>
								<input id="sSearch_1" name="sSearch_1" type="text" class="searchInput  form-control">
							</div>
							<div class="col-md-3 col-sm-13 col-lg-3 col-xs-12 dataTables_filter">
								<label>Location Address</label>
								<input id="sSearch_2" name="sSearch_2" type="text" class="searchInput  form-control">
							</div>
							<div class="col-md-3 col-sm-12 col-lg-3 col-xs-12 dataTables_filter">
								<label>Status</label>
								<select id="sSearch_3" name="sSearch_3" class="searchInput form-control show-tick" width="100%">
									<option value="">All Status</option>
									<option value="">All Status</option>
									<option value="Active">Active</option>
									<option value="In-active">In-active</option>
								</select>
							</div>
							<div class="col-md-3 col-sm-13 col-lg-3 col-xs-12">
								<button class="btn btn-primary mt25 waves-effect" onclick="clearSearchFilters();">Clear Search</button>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="body desktop_data">
						<div class="box-content">
							<table cellpadding="0" cellspacing="0" border="0" class=" dynamicTable table table-bordered" width="100%">
								<thead>
									<tr>
										<th>Location Name</th>
										<th>Location Code</th>
										<th>Location Address</th>
										<th>Status</th>
										<th data-bSortable = "false" >Change Status</th>
										<th>Actions</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
								<tfoot>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
			<hr>
		</div>	
		<!-- #END# Widgets -->
	</div>
</section>
<script>
function changeStatus(user_id,elem) {
	if($(elem).is(":checked") == true){
		$(elem).prop("checked",false);
	}else{
		$(elem).prop("checked",true);
	}
	swal({
		title: "Are you sure?",
		text: "To Change the Status.",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#a81f3b",
		confirmButtonText: "Yes, Change it !",
		closeOnConfirm: false
	}, function () {
		$.ajax({
			url: "<?php echo base_url($this->router->fetch_module());?>/changeStatus",
			async: false,
			data : { id : user_id},
			type: "POST",
			dataType: "json",
			success: function (response){
				if(response.success){
					swal({title: "Success!",text: "Status Changed Successfully.",confirmButtonColor: "#a81f3b",imageUrl: "<?php echo base_url('assets/images/thumbs-up.png');?>"});
					if($(elem).is(":checked") == true){
						$(elem).prop("checked",false);
					}else{
						$(elem).prop("checked",true);
					}
					clearSearchFilters();
				}else{
					swal("Problem in Changing Status!");
				}
			}
		});
		
	});
}
</script>