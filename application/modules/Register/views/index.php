<div class="maincontent-area">
  <div class="container">
    <div class="row">
      <?php if($register_uer == "exporter"){ ?>
      <div class="panel panel-primary registration-panel ">
        <div class="panel-heading">
          <div class="col-sm-12 text-center">
            <h2>
              Exporter Registration
            </h2>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="panel-body">
          <form action="#" name="exporter_register_form"  id="registration_form" method="post" enctype="multipart/form-data" >
            <input type="hidden" name="user_type" id="user_type" value="<?php echo $register_uer;?>">
            <div class="form-group">
              <label for="">Company Name <spane class="text-danger">*</span></label>
              <input type="text" name="exporter_company_name" id="exporter_company_name" class="form-control">
            </div>
            <div class="form-group">
              <label for="">Phone Number <spane class="text-danger">*</span></label>
              <input type="text" name="exporter_phone_number" id="exporter_phone_number" class="form-control">
            </div>
            <div class="form-group">
              <label for="">APEDA Certificate <spane class="text-danger">*</span></label>
              <input type="file" name="apeda_file" id="apeda_file" class="form-control">
            </div>
            <div class="form-group">
              <label for="">Username <spane class="text-danger">*</span></label>
              <input type="text" name="username" id="username" class="form-control">
            </div>
            <div class="form-group">
              <label for="">Password <spane class="text-danger">*</span></label>
              <input type="password" name="password" id="password" class="form-control">
            </div>
            <div class="form-group">
              <label for="">Confirm Password <spane class="text-danger">*</span></label>
              <input type="password" name="confirm_password" id="confirm_password" class="form-control">
            </div>
            <!-- <div class="form-group">
              <label for="">
                <input type="checkbox" name="" id="">
                I agree terms and conditions.
              </label>
            </div> -->
            <div class="form-group text-center">
              <button class="btn btn-primary" type="submit" >Register</button>
              <a class="btn btn-primary" href="<?php echo base_url();?>">Cancel</a>
            </div>
          </form>
        </div>
      </div>
      <?php }else{ ?>
      <div class="panel panel-primary registration-panel ">
        <div class="panel-heading">
          <div class="col-sm-12 text-center">
            <h2>
              Buyer Registration
            </h2>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="panel-body">
          <form action="#" name="buyer_register_form"  id="registration_form" method="post" enctype="multipart/form-data">
            <input type="hidden" name="user_type" id="user_type" value="<?php echo $register_uer;?>">
            <div class="form-group">
              <label for="">Name <spane class="text-danger">*</span></label>
              <input type="text" name="buyer_name" id="buyer_name" class="form-control">
            </div>
            <div class="form-group">
              <label for="">Company Name <spane class="text-danger">*</span></label>
              <input type="text" name="buyer_company_name" id="buyer_company_name" class="form-control">
            </div>
            <div class="form-group">
              <label for="">Phone Number <spane class="text-danger">*</span></label>
              <input type="tel" name="buyer_phone_number" id="buyer_phone_number" class="form-control">
            </div>
            <div class="form-group">
              <label for="">Username <spane class="text-danger">*</span></label>
              <input type="text" name="username" id="username" class="form-control">
            </div>
            <div class="form-group">
              <label for="">Password <spane class="text-danger">*</span></label>
              <input type="password" name="password" id="password" class="form-control">
            </div>
            <div class="form-group">
              <label for="">Confirm Password <spane class="text-danger">*</span></label>
              <input type="password" name="confirm_password" id="confirm_password" class="form-control">
            </div>
            <div class="form-group text-center">
              <button class="btn btn-primary" type="submit">Register</button>
              <a class="btn btn-primary" href="<?php echo base_url();?>">Cancel</a>
            </div>
          </form>
        </div>
      </div>
      <?php } ?>
    </div>
  </div>
</div>
<script>
$(document).ready(function () {
    var register_type = "<?php echo $register_uer; ?>";
    if(register_type == "exporter"){
      var rules_obj = {
        exporter_company_name:{required:true},
        exporter_phone_number:{required:true},
        apeda_file:{required:true},
        username:{
          required:true,
          remote: {
            url:"<?php echo base_url($this->router->fetch_module());?>/checkExisting" ,
            type: "post",
            data: {
              username: function() {
                return $( "#username" ).val();
              },user_type:function() {
                return $( "#user_type" ).val();
              }
            }
          }
        },
        password:{required:true},
        confirm_password:{required:true},
      }
      var msg_obj = {
        exporter_company_name:{required:"Please Enter Company Name"},
        exporter_phone_number:{required:"Please Enter Phone Number"},
        apeda_file:{required:"Please upload APEDA Certificate"},
        username:{required:"Please Enter Username",remote:"Username already exist please Enter different name."},
        password:{required:"Please Enter Password"},
        confirm_password:{required:"Please Enter Confirm Password."},
      }
    }else{
      var rules_obj = {
        buyer_name:{required:true},
        buyer_company_name:{required:true},
        buyer_phone_number:{required:true},
        username:{
          required:true,
          remote: {
            url:"<?php echo base_url($this->router->fetch_module());?>/checkExisting" ,
            type: "post",
            data: {
              username: function() {
                return $( "#username" ).val();
              },user_type:function() {
                return $( "#user_type" ).val();
              }
            }
          }
        },
        password:{required:true},
        confirm_password:{required:true},
      }
      var msg_obj = {
        buyer_name:{required:"Please Enter Name"},
        buyer_company_name:{required:"Please Enter Company Name"},
        buyer_phone_number:{required:"Please Enter Phone Number"},
        apeda_file:{required:"Please upload APEDA Certificate"},
        username:{required:"Please Enter Username",remote:"Username already exist please Enter different name."},
        password:{required:"Please Enter Password"},
        confirm_password:{required:"Please Enter Confirm Password."},
      }
    }
    var ajaxUrl = "<?php echo base_url("register/submitForm"); ?>";
    $("#registration_form").validate({
        rules: rules_obj,
        messages: msg_obj,
        submitHandler: function (form) {
          $(form).ajaxSubmit({
            url: ajaxUrl, 
            type: 'POST',
            dataType: 'json',
            cache: false,
            clearForm: false, 
            beforeSubmit : function(arr, $form, options){
              $(".btn-primary").hide();
            },
            success: function (response) 
            {
              $(".btn-primary").show();
              if(response.success)
              {
                swal({title: "Success!",text:response.msg,confirmButtonColor: "#0072ce",imageUrl: "<?php echo base_url('assets/images/thumbs-up.png');?>"});
                setTimeout(function(){
                  window.location = "<?php echo base_url($this->router->fetch_module());?>";
                },2000);
              }else{	
                swal(response.msg);
                return false;
              }
            }
          });
        }
    });

});

$(document).ready(function(){
  var buyer_input = document.querySelector("#buyer_phone_number");
  window.intlTelInput(buyer_input, {
    preferredCountries: ['in','cn','th']
  });

  var exporter_input = document.querySelector("#exporter_phone_number");
  window.intlTelInput(exporter_input, {
    preferredCountries: ['in','cn','th']
  });
})
</script>