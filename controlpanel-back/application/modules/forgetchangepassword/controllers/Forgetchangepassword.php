<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// session_start(); //we need to call PHP's session object to access it through CI
class Forgetchangepassword extends CI_Controller 
{
	function __construct()
	{
		// print_r("expression");
		// exit;
	   parent::__construct();
	   $this->load->model('forgetchangepasswordmodel','',TRUE);
	   $this->load->model('common_model/common_model','common',TRUE);
		if(!empty($_SESSION["jain_decorator_admin"])){
			redirect('home/index', 'refresh');
		}
	}
 
	function index()
	{   
if(!empty($_GET['text']) && isset($_GET['text'])){
			$email_user = array();
			$email_user = explode('/',$_GET['text']);
			
			$varr=base64_decode(strtr($email_user[0], '-_', '+/'));
			parse_str($varr,$url_prams);
			$email_id = $url_prams['eid'];		
			$varr1=base64_decode(strtr($email_user[1], '-_', '+/'));	
			parse_str($varr1,$url_prams1);
				$url_date = $url_prams1['dt'];
				//echo $url_date; exit;
			if($url_date == date("Y-m-d")){
				if(!empty($email_id)){
					$result = $this->forgetchangepasswordmodel->checkUserDetails($email_id);
					//echo "<pre>";
					//print_r($result);
					//exit;
					if($result){
						$data = "";
						$data['email_id'] =  $email_id;
						
						$this->load->view('template/login_header.php');
						$this->load->view('forgetchangepassword/addEdit',$data);
						$this->load->view('template/footer.php');
					}else{
						redirect('home', 'refresh');
					}
				}else{
					redirect('home', 'refresh');
				}
			}else{
				redirect('login', 'refresh');
			}
		}else{
			redirect('login', 'refresh');
		}
	}
 
	function submitForm()
	{
		// print_r("expression");
		// exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			if(!empty($_POST['email_id'])){
				$result = $this->forgetchangepasswordmodel->checkUserDetails($_POST['email_id']);
				if($result){							
					$data = array();
					$data['password'] = md5($_POST['newpassword']);
					//$data['first_name'] = $_POST['newpassword'];
					$result = $this->forgetchangepasswordmodel->updateRecord($data,$_POST['email_id']);		
					// print_r($result);
					// exit;				
					if(!empty($result)){
						// print_r("expression");
						// exit;
						echo json_encode(array('success'=>true, 'msg'=>'Password Changed Successfully. Please Login'));
						exit;
					}else{
						echo json_encode(array('success'=>false, 'msg'=>'Problem in data update.'));
						exit;
					}
				}else	{
					echo json_encode(array('success'=>false, 'msg'=>'Problem in data update.'));
					exit;
				}				
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Problem in data update.'));
				exit;
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem in data update.'));
			exit;
		}
	}
}

?>