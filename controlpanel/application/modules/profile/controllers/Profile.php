<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Profile extends CI_Controller 
{	
	function __construct(){
		parent::__construct();
		$this->load->model('profilemodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
		checklogin();
	}
 
	function index(){
	    $result =array();
	    $users_data = $this->common->getData("tbl_users","*",array("user_id"=>$_SESSION['sevasadan_control_admin'][0]['user_id']));
	    $result['users_data'] = $users_data[0];
		$this->load->view('template/header.php');
		$this->load->view('profile/index',$result);
		$this->load->view('template/footer.php');
	}
	function submitForm(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			$data['first_name'] = $this->input->post('first_name');
			$data['last_name'] = $this->input->post('last_name');
			$data['email_id'] = $this->input->post('email_id');
			$data['mobile'] = $this->input->post('mobile');
			$data['address'] = $this->input->post('address');
			if(!empty($this->input->post("user_id"))){
			    if(!empty($this->input->post('change_credentials')) && $this->input->post('change_credentials') == "on"){
			        $data['username'] = $this->input->post('username');
			    	$data['password'] = md5($this->input->post('password'));
			    }
				$result = $this->common->updateData("tbl_users",$data,array("user_id"=>$this->input->post("user_id")));
				if($result){
					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while Updating data.'));
					exit;
				}
			}else{
				$data['username'] = $this->input->post('username');
				$data['password'] = md5($this->input->post('password'));
				$result = $this->common->insertData("tbl_users",$data,"1");
				if($result){
					echo json_encode(array('success'=>true, 'msg'=>'Record Inserted Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while Inserting data.'));
					exit;
				}
			}
		}
	}
	
	function dataExist(){
		if(!empty($this->input->post("current_password")) && !empty($this->input->post("id"))){
			$result = $this->common->getData("tbl_users","user_id",array("user_id"=>$this->input->post("id"),"password"=>md5($this->input->post("current_password")),"status"=>"Active"));
			if(!empty($result)){
				echo json_encode(TRUE);
				exit;
			}else{
				echo json_encode(FALSE);
				exit;
			}
		}
	}
	
}

?>
