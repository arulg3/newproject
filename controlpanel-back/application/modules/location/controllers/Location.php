<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Location extends CI_Controller 
{	
	function __construct(){
		parent::__construct();
		$this->load->model('locationmodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
		checklogin();
	}
 
	function index(){
		$this->load->view('template/header.php');
		$this->load->view('location/index');
		$this->load->view('template/footer.php');
	}
	function addEdit(){
		$record_id = "";
		$edit_datas = array();
		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$record_id = $url_prams['id'];
			$result = $this->common->getData("tbl_location","*",array("location_id"=>$record_id));
			$edit_datas['location_users_data'] = $this->common->getData("tbl_location_users","*",array("location_id"=>$record_id));
			if(!empty($result)){
				$edit_datas['location_data'] = $result[0];
			}
		}
		// $edit_datas['user_data']= $this->common->getData("tbl_users","*",array("status"=>"Active","user_type"=>"2"));
		$edit_datas['user_data']= $this->common->getData("tbl_users","*");
		$this->load->view('template/header.php');
		$this->load->view('location/addEdit',$edit_datas);
		$this->load->view('template/footer.php');
	}
	function fetch(){
		$get_result = $this->locationmodel->getRecords($_GET);
		$result = array();
		$result["sEcho"]= $_GET['sEcho'];
		$result["iTotalRecords"] = $get_result['totalRecords'];	//iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"]= $get_result['totalRecords']; //iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result']) && count($get_result['query_result']) > 0){
			for($i=0;$i<sizeof($get_result['query_result']);$i++){
				$temp = array();
				array_push($temp, ucfirst($get_result['query_result'][$i]->location_name));
				array_push($temp, $get_result['query_result'][$i]->location_code);
				array_push($temp, $get_result['query_result'][$i]->location_address);
				array_push($temp, $get_result['query_result'][$i]->status);
				
				$status_change = "";
				$actionCol = "";
				$status_type = '';
				if($get_result['query_result'][$i]->status == 'Active'){
					$status_type = "checked = 'checked'";
				}
				
				$status_change = '<div style="width:75px;float:right"><div class="switch">
									<label>
										<input type="checkbox" class="status_change" onchange="changeStatus('.$get_result['query_result'][$i]->location_id.',this)"  '.$status_type.'">
										<span class="lever"></span>
									</label>
								</div></div>';
				array_push($temp, $status_change);
				
				$actionCol .='<div style="width:75px;float:right;text-align: center;"><a href="location/addEdit?text='.rtrim(strtr(base64_encode("id=".$get_result['query_result'][$i]->location_id), '+/', '-_'), '=').'" title="Edit" class="text-center"><i class="material-icons ">create</i></a></div>';
				array_push($temp, $actionCol);
				
				array_push($items, $temp);
			}
		}
		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}
	
	function submitForm(){
		// echo "<pre>";
		// print_r($_POST);
		// exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{

			/*check duplicate entry*/	
			$condition = "location_name = '".$this->input->post('location_name')."' ";
			if(!empty($this->input->post("location_id"))){
				$condition .= " AND location_id <> ".$this->input->post("location_id");
			}	

			$chk_location_sql = $this->common->Fetch("tbl_location","location_id",$condition);
			$rs_location = $this->common->MySqlFetchRow($chk_location_sql, "array");

			if(!empty($rs_location[0]['location_id'])){
				echo json_encode(array('success'=>false, 'msg'=>'Company name already exist...'));
				exit;
			}

			$data = array();
			$data['location_name'] = $this->input->post('location_name');
			$data['location_code'] = $this->input->post('location_code');
			$data['location_address'] = trim($this->input->post('location_address'));
			if(!empty($this->input->post("location_id"))){
				$data['updated_by'] = $_SESSION['jain_decorator_admin'][0]['user_id'];
				$data['updated_on'] = date("Y-m-d H:i:s");
				$result = $this->common->updateData("tbl_location",$data,array("location_id"=>$this->input->post("location_id")));
				if($result){
					/* remove existing loaction users */
					$this->common->deleteRecord("tbl_location_users",array("location_id"=>$this->input->post("location_id")));
					if(!empty($this->input->post("user_id"))){
						$subdata = array();
						$user_ids = $this->input->post("user_id");
						foreach($user_ids as $key=>$val){
							$subdata['location_id'] = $this->input->post("location_id");
							$subdata['user_id'] = $val;
							//print_r($subdata);
	
							$this->common->insertData("tbl_location_users",$subdata,"1");
						}
					}
					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while Updating data.'));
					exit;
				}
			}else{
				$data['created_by'] = $_SESSION['jain_decorator_admin'][0]['user_id'];
				$data['created_on'] = date("Y-m-d H:i:s");
				$result = $this->common->insertData("tbl_location",$data,"1");
				if($result){
					if(!empty($this->input->post("user_id"))){
						$subdata = array();
						$user_ids = $this->input->post("user_id");
						//print_r($user_ids);
						foreach($user_ids as $key=>$val){
							$subdata['location_id'] = $result;
							$subdata['user_id'] = $val;
							//print_r($subdata);
							$this->common->insertData("tbl_location_users",$subdata,"1");
						}
					}
					echo json_encode(array('success'=>true, 'msg'=>'Record Inserted Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while Inserting data.'));
					exit;
				}
			}
		}
	}
	
	function changeStatus(){
		if(!empty($this->input->post("id"))){
			$existing_data = $this->common->getData("tbl_location","status",array('location_id'=>$this->input->post("id")));
			$data = array();
			if(!empty($existing_data) && $existing_data[0]['status'] == "Active"){
				$data['status'] = "In-active";
			}else{
				$data['status'] = "Active";
			}
			$result = $this->common->updateData("tbl_location",$data,array("location_id"=>$this->input->post("id")));
			if($result){
				echo json_encode(array('success'=>true));
				exit;
			}else{
				echo json_encode(array('success'=>false));
			}
		}
	}
	
	function dataExist(){
		$condition = array("location_code"=>$_POST['location_code']);

		if(!empty($_POST['location_id']) && $_POST['location_id'] !=""){
			$condition['location_id <>'] = $_POST['location_id'];
		}

		$result=$this->common->getData("tbl_location","*",$condition);
				
		// echo $this->db->last_query();
		// echo "<pre>";
		// print_r($result);
		// exit;
				
		if($result > 0){
			echo  json_encode(FALSE);
		}else{
			echo  json_encode(TRUE);
		} 
	}
}

?>
